#!/bin/bash

set -e


if [ -z $TARGET_VERSION ]; then
    echo "TARGET_VERSION is not available"
    exit -1
fi

npm install
npm run test-headless
npm run build
docker build --build-arg TARGET_VERSION=$TARGET_VERSION -t xivoxc/webpanels:$TARGET_VERSION -f docker/Dockerfile .