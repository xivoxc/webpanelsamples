var webpackConfig = require('./webpack.config.js');

delete webpackConfig.entry;
webpackConfig.plugins.splice(2);
//For debugging only as too slow for tdd, see https://webpack.js.org/configuration/devtool/
//webpackConfig.devtool='inline-source-map';

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],

    reporters: ['progress'],
    port: 9876,
    colors: false,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeHeadless'],
    singleRun: false,
    autoWatchBatchDelay: 300,

    files: [
      './node_modules/jquery/dist/jquery.js',
      './node_modules/angular/angular.js',
      './node_modules/angular-mocks/angular-mocks.js',
      './app/scripts/xccti/js/cti.js',      
      './test/mocks/mockobjectsbuilder.js',
      './test/mocks/testHelpers.js',
      './test/spec/panels/**/*.spec.js',
      './app/scripts/Th2/app.js',
      './app/scripts/chronoflex/app.js',
      './test/karma-bootstrap.js',
      './app/**/*.html',
      './test/spec/Th2/services/*.spec.js',
      './test/spec/Th2/controllers/*.spec.js',
      './test/spec/chronoflex/controllers/*.spec.js',
      {pattern: './app/data/**/*', watched: false, served: true, included: false},
    ],

    proxies: {
      "data/" : "/base/app/data/"
    },

    preprocessors: {
      './app/scripts/panels/panels-webpack.js': ['webpack', 'sourcemap'],
      './app/scripts/chronoflex/app.js': ['webpack', 'sourcemap'],
      './app/scripts/Th2/app.js': ['webpack', 'sourcemap'],
      './test/spec/**/*.spec.js': ['webpack', 'sourcemap']
    },

    webpack: webpackConfig,

    webpackMiddleware: {
      noInfo: 'errors-only'
    },

    ngHtml2JsPreprocessor: {
      stripPrefix: 'app/',
      moduleName: 'html-templates'
    }
  });
};
