/*
*
*  ugps application
*/
import 'angular';

import 'xccti/cti-webpack';
import 'panels/panels-webpack';

import '../../styles/agentstates.css';
import '../../styles/panels/ugps_3.css';
import '../../styles/panels/ugpscolor.css';
import '../../styles/seuils.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

angular.module('ugps_3',['panels']);

