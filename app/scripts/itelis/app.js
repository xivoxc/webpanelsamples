
import 'angular';

import 'xccti/cti-webpack';
import 'panels/panels-webpack';

import '../../styles/agentstates.css';
import '../../styles/panels/itelis.css';
import '../../styles/seuils.css';

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

angular.module('itelis',['panels']);