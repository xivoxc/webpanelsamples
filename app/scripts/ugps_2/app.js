/*
*
*  ugps application
*/
import 'angular';

import 'xccti/cti-webpack';
import 'panels/panels-webpack';

import '../../styles/agentstates.css';
import '../../styles/panels/ugps_2.css';
import '../../styles/seuils.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

angular.module('ugps_2',['panels']);

