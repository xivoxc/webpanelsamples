/*
*
*  blackboard application
*/
import 'angular';

import 'xccti/cti-webpack';
import 'panels/panels-webpack';

import '../../styles/agentstates.css';
import '../../styles/optpanel.css';
import '../../styles/seuils.css';
import '../../styles/optpanel-sm.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

angular.module('blackboard',['panels']);


