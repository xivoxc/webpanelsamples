import 'angular';

import 'xccti/cti-webpack';
import 'panels/panels-webpack';

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import '../../styles/seuils.css';

import '../../styles/panels/chronoflex.css';
import CallbacksTotalController from './controllers/callbackstotal.controller';

angular.module('chronoflex',['panels'])
    .controller('CallbacksTotalController',CallbacksTotalController);