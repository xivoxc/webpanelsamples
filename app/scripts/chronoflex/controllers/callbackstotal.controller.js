export default class CallbacksTotalController {
  constructor(XucCallback, $rootScope, $scope, $log) {
    $log.info('CallbacksTotal controller init');

    this.$log = $log;
    this.xucCallback = XucCallback;
    this.$scope = $scope;
    this.$rootScope = $rootScope;

    this.$scope.callbackLists = [];
    this.$scope.callbacksCount = 0;


    this.$scope.$on('CallbacksLoaded', this.refreshCallbacks.bind(this));
    this.$scope.$on('CallbackTaken', this.refreshCallbacks.bind(this));
    this.$scope.$on('CallbackReleased', this.refreshCallbacks.bind(this));
    this.$scope.$on('CallbackClotured', this.refreshCallbacks.bind(this));

    this.$scope.$watch('queues', this.loadCallbacks.bind(this));
  }

  _checkFilteredQueues(list) {
    var result = false;
    angular.forEach(this.$rootScope.queues, function(queue) {
      if (queue.id === list.queueId) {
        result = true;
      }
    });
    return result;
  }

  loadCallbacks() {
    this.xucCallback.getCallbackListsAsync();
  }

  refreshCallbacks() {
    this.$log.debug('refreshCallbacks');
    this.$scope.callbacksCount = 0;
    this.$scope.callbackLists = this.xucCallback.getCallbackLists().filter(this._checkFilteredQueues.bind(this));
    this.$scope.callbackLists.forEach((list) => {
      list.callbacks.forEach(() => {
        this.$scope.callbacksCount++;
      });
    });

    this.$scope.atLeastOneCallback = (this.$scope.callbacksCount !== 0);

  }
}
