/*
*
*  ugps application
*/
import 'angular';

import 'xccti/cti-webpack';
import 'panels/panels-webpack';

import '../../styles/agentstates.css';
import '../../styles/panels/ugps.css';
import '../../styles/seuils.css';
import '../../styles/panels/ugpscolor.css';

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

angular.module('ugps',['panels']);

