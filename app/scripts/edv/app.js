/*
*
*  perqueuelogo application
*/
import 'angular';

import 'xccti/cti-webpack';
import 'panels/panels-webpack';

import '../../styles/panels/edv.css';

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import '../../../node_modules/chart.js';
import '../../../node_modules/angular-chart.js';

import '../../styles/agentstates.css';
angular.module('edv',['panels','chart.js']);


