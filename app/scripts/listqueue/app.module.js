import angular from 'angular';
import ListQueueController from './controllers/listqueue.controller';

angular.module('listqueue',['panels'])
  .controller('ListQueueController',ListQueueController)
;