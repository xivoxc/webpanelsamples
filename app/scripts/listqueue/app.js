import 'angular';

import 'xccti/cti-webpack';
import 'panels/panels-webpack';

import '../../styles/panels/listqueue.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

import './app.module';
import './controllers/listqueue.controller';

