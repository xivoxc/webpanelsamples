export default class ListQueueController {
  constructor(XucQueue, $scope, PanelQueueTotal, $timeout, Preferences,QueueGroup, $log) {
    this.updateQueueFrequency = 15;
    this.xucQueue = XucQueue;
    this.$scope = $scope;
    this.panelQueueTotal = PanelQueueTotal;
    this.$timeout = $timeout;
    this.preferences = Preferences;
    this.queues = [];
    this.$log = $log;
    this.queueGroup=QueueGroup;

    this.panelColor = this.preferences.getPanelColor();
    this.stats = [];

    this.$timeout(() => this.getQueues(), this.updateQueueFrequency * 1000, false);
    
    this.$scope.$on('ctiLoggedOn', () => {
      this.panelColor = this.preferences.getPanelColor();
      this.preferences.init();
      this.xucQueue.start();

    });

    this.$scope.$on('linkDisConnected', () => this.queues);

  }

  getQueues() {
    const _groupDefs = this.preferences.getQueueGroups();
    this._groups = [];
    this._tempQueue = [];
    for(let g of _groupDefs) {
      angular.forEach(g.queueids, (queuid) => {
        this._groups.push(queuid);
      });
      
    }

    this._tempQueue=this.xucQueue.getQueues();
    if(this._groups.length === 0){
      this.queues=this._tempQueue;
    }else
    {
      for (var i = 0; i <  this._tempQueue.length; i++) {
        if (this._groups.includes(this._tempQueue[i].id)){ 
          this.queues.push(this._tempQueue[i]);
        }
      }
    }

  }
}