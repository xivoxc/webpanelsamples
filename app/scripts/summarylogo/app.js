/*
*
*  summarylogo application
*/
import 'angular';

import 'xccti/cti-webpack';
import 'panels/panels-webpack';

import '../../styles/agentstates.css';
import '../../styles/panels/summarylogo.css';
import '../../styles/seuils.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

angular.module('summarylogo',['panels']);


