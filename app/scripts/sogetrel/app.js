
/*
*
*  sogetrel application
*/

import 'angular';

import 'xccti/cti-webpack';
import 'panels/panels-webpack';

import '../../styles/agentstates.css';

import '../../styles/panels/sogetrel.css';
import '../../styles/seuils.css';

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import '../../../node_modules/chart.js';
import '../../../node_modules/angular-chart.js';


angular.module('sogetrel',['panels', 'chart.js']);




