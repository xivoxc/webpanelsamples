/*
*
*  globaloptpanel application
*/
import 'angular';

import 'xccti/cti-webpack';
import 'panels/panels-webpack';

import '../../styles/agentstates.css';
import '../../styles/optpanel.css';
import '../../styles/seuils.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-select/dist/css/bootstrap-select.min.css';
import 'bootstrap-select' ;

angular.module('globaloptpanel',['panels', 'bootstrap-select']);


