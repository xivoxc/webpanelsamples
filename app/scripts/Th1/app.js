/*
*
*  Th1 application
*/
import 'angular';

import 'xccti/cti-webpack';
import 'panels/panels-webpack';

import '../../styles/agentstates.css';
import '../../styles/lfm.css';
import '../../styles/panels/Th1.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

angular.module('Th1',['panels']);


