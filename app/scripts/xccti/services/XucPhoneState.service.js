import _ from 'lodash';

export default function XucPhoneState($rootScope, XucPhoneEventListener, XucLink) {
  var _STATE_DIALING = "Dialing";
  var _STATE_RINGING = "Ringing";
  var _STATE_ESTABLISHED = "Established";
  var _STATE_RELEASED = "Released";
  var _STATE_ONHOLD = "OnHold";

  var _state = 'UNKNOWN';

  var _eventStates = {};
  _eventStates[XucPhoneEventListener.EVENT_DIALING] = _STATE_DIALING;
  _eventStates[XucPhoneEventListener.EVENT_RINGING] = _STATE_RINGING;
  _eventStates[XucPhoneEventListener.EVENT_ESTABLISHED] = _STATE_ESTABLISHED;
  _eventStates[XucPhoneEventListener.EVENT_RELEASED] = _STATE_RELEASED;
  _eventStates[XucPhoneEventListener.EVENT_ONHOLD] = _STATE_ONHOLD;

  var _calls = [];
  var _conference = {calls: [], active: false};

  var _getState = function() {
    return _state;
  };

  var _getCalls = function() {
    return _calls;
  };

  var _isInCall = function() {
    return _calls.length !== 0;
  };

  var _getConference = function() {
    return _conference;
  };

  var _dial = function(number) {
    if(_calls.length === 0) {
      Cti.dial(number);
    } else {
      Cti.attendedTransfer(number);
    }
  };

  var _isPhoneAvailable = function(status) {
    return status === "AVAILABLE";
  };

  var _isPhoneOffHook = function(status) {
    return status === "ONHOLD" ||
      status === "BUSY_AND_RINGING" ||
      status === "CALLING" ||
      status === "BUSY";
  };

  var _isPhoneRinging = function(status) {
    return status === "RINGING";
  };

  var _statusHandler = function(event) {
    _state = event.status;
    if(_state === 'AVAILABLE') {
      _calls.splice(0,_calls.length);
    }
    $rootScope.$broadcast('phoneStateUpdated', _state);
  };

  var _findLineIndex = function(_calls, value) {
    return _.findIndex(_calls, function(o) { return o.linkedId === value; });
  };

  var _unregisterOnPhone;
  var _onPhoneEvent = function(event) {
    var lineIndex = _findLineIndex(_calls, event.linkedId);
    if(lineIndex < 0 && event.eventType === XucPhoneEventListener.EVENT_RELEASED) {
      lineIndex = _findLineIndex(_calls, event.uniqueId);
    }
    if (lineIndex < 0) {
      lineIndex = _calls.length;
      _calls[lineIndex] = { linkedId: event.linkedId, direction: 'outgoing', acd: false };
    }

    _calls[lineIndex].state = _eventToState(event.eventType);
    _calls[lineIndex].uniqueId = event.uniqueId;
    _calls[lineIndex].DN = event.DN;

    if(event.eventType === XucPhoneEventListener.EVENT_RINGING) {
      _calls[lineIndex].direction = 'incoming';
    }
    
    if(typeof(event.otherDN) !== "undefined" && event.otherDN !== "") {
      _calls[lineIndex].otherDN = event.otherDN;
    }
    if(typeof(event.otherDName) !== "undefined" && event.otherDName !== "") {
      _calls[lineIndex].otherDName = event.otherDName;
    }
    if(typeof(event.queueName) !== "undefined" && event.queueName !== "") {
      _calls[lineIndex].queueName = event.queueName;
      _calls[lineIndex].acd = true;
    }
    _calls[lineIndex].userData = event.userData;

    if(typeof(_calls[lineIndex].startTime) === "undefined" && event.eventType === XucPhoneEventListener.EVENT_ESTABLISHED) {
      _calls[lineIndex].startTime = Date.now();
    }

    if(event.eventType === XucPhoneEventListener.EVENT_RELEASED) {
      _.remove(_calls, function(call) {
        return call.uniqueId === event.uniqueId || call.linkedId === event.linkedId;
      });
    }

    if(_calls.length > 1 && _.every(_calls, {state: _STATE_ESTABLISHED})) {
      if(!_conference.active) {
        _conference.startTime = Date.now();
        _conference.active = true;
      }
      _.each(_calls, function(call) {
        if(!_.find(_conference.calls, {uniqueId: call.uniqueId})) {
          _conference.calls.push(call);
        }
      });
    } else {
      _conference.active = false;
      _conference.calls.length = 0;
    }
  };

  var _eventToState = function(state) {
    if(typeof(_eventStates[state]) !== "undefined") {
      return _eventStates[state];
    } else {
      return null;
    }
  };

  var _unInit = function() {
    _calls = [];
    _conference = {calls: [], active: false};
    if (typeof _unregisterOnPhone === "function") {
      _unregisterOnPhone();
      _unregisterOnPhone = undefined;
    }
    XucLink.whenLogged().then(_init);
  };

  var _init = function() {
    Cti.setHandler(Cti.MessageType.PHONESTATUSUPDATE, _statusHandler);
    if (_unregisterOnPhone === undefined) {
      _unregisterOnPhone = XucPhoneEventListener.addHandlerCustom(_onPhoneEvent);
    }
    XucLink.whenLoggedOut().then(_unInit);
  };

  XucLink.whenLogged().then(_init);

  return {
    STATE_DIALING: _STATE_DIALING,
    STATE_RINGING: _STATE_RINGING,
    STATE_ESTABLISHED: _STATE_ESTABLISHED,
    STATE_RELEASED: _STATE_RELEASED,
    STATE_ONHOLD: _STATE_ONHOLD,
    getState: _getState,
    getCalls: _getCalls,
    isInCall: _isInCall,
    getConference: _getConference,
    dial: _dial,
    isPhoneAvailable: _isPhoneAvailable,
    isPhoneOffHook: _isPhoneOffHook,
    isPhoneRinging: _isPhoneRinging,
    _statusHandler: _statusHandler
  };
}
