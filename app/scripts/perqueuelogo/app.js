/*
*
*  perqueuelogo application
*/
import 'angular';

import 'xccti/cti-webpack';
import 'panels/panels-webpack';

import '../../styles/panels/perqueuelogo.css';
import '../../styles/panels/perqueuecolors.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

angular.module('perqueuelogo',['panels']);


