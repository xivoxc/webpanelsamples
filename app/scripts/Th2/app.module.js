import angular from 'angular';
import QueueController from './controllers/queue.controller';

angular.module('Th2',['panels'])
  .controller('QueueController',QueueController)
;