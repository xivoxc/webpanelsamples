export default class QueueController {
  constructor(XucQueue, $scope, PanelQueueTotal, $timeout, Preferences, QueueSelector, $log) {
    this.updateQueueFrequency = 15;
    this.xucQueue = XucQueue;
    this.$scope = $scope;
    this.panelQueueTotal = PanelQueueTotal;
    this.$timeout = $timeout;
    this.preferences = Preferences;
    this.queueSelector = QueueSelector;
    this.queues = [];
    this.$log = $log;

    this.panelColor = this.preferences.getPanelColor();
    this.stats = [];

    this.$timeout(() => this.getQueues(), this.updateQueueFrequency * 1000, false);
    this.$timeout(() => this.getStats(), this.updateQueueFrequency * 1000, false);
    
    this.$scope.$on('ctiLoggedOn', () => {
      this.panelColor = this.preferences.getPanelColor();
      this.xucQueue.start();
    });

    this.$scope.$on('linkDisConnected', () => this.queues);

  }

  getStats() {
    this.panelQueueTotal.calculate(this.queues);
    this.stats = this.panelQueueTotal.getCalcStats();
    this.$timeout(() => this.getStats(), this.updateQueueFrequency * 1000, false);
  }
  getQueues() {
    this.queueSelector.getQueues().then(queues => this.queues = queues);
  }
}