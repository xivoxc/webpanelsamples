(function () {
  'use strict';

  angular
    .module('Th2')
    .factory('QueueSelector', QueueSelector);

  QueueSelector.$inject = ['DataFileLoader', 'Preferences', 'ObjectSorter', 'XucQueue', '$q'];

  function QueueSelector(dataFileLoader, preferences, objectSorter, xucQueue, $q) {

    var _getQueues = function () {
      var name = preferences.getSelectedQueueName();
      if (name !== "") {
        return $q.all({queueNames: dataFileLoader.loadQueuesList(name),availableQueues: xucQueue.getQueuesAsync()})
          .then(function (resolutions) {
            return objectSorter.sort(resolutions.queueNames, resolutions.availableQueues);
          });
      } else {
        return xucQueue.getQueuesAsync().then(function (queues) {
          return queues;
        });
      }
    };

    return {
      getQueues: _getQueues
    };

  }
})();