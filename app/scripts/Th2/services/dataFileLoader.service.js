(function () {
  'use strict';

  angular
    .module('Th2')
    .factory('DataFileLoader', DataFileLoader);

  DataFileLoader.$inject = ['$http'];

  function DataFileLoader($http) {

    var _loadQueuesList = function (name) {
      return $http.get('data/selectQueues.json').then(function (response) {
        var queues;
        angular.forEach(response.data, function (value) {
          if (value.name === name) {
            queues = value.queues;
          }
        });
        return queues;
      });
    };

    return {
      loadQueuesList: _loadQueuesList
    };

  }
})();