(function () {
  'use strict';

  angular
    .module('Th2')
    .factory('ObjectSorter', ObjectSorter);

  function ObjectSorter() {

    var _sort = function (filter, list) {
      var res = [];
      if (filter === null) {
        return list;
      }
      angular.forEach(filter, function (testQueue) {
        angular.forEach(list, function (queue) {
          if (testQueue === queue.name) {
            res.push(queue);
          }
        });
      });
      return res;
    };

    return {
      sort: _sort
    };

  }
})();