import 'angular';

import 'xccti/cti-webpack';
import 'panels/panels-webpack';

import '../../styles/agentstates.css';
import '../../styles/lfm.css';
import '../../styles/panels/Th2.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

import './app.module';
import './services/dataFileLoader.service';
import './services/objectSorter.service';
import './services/queueSelector.service';
import './controllers/queue.controller';

