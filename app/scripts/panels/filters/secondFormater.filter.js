export default function secondFormater() {
  return function (seconds) {
    seconds = 0 + seconds;
    if (typeof seconds !== 'number') return '';
    var hour = (seconds / 3600 % 12) << 0;
    hour = (hour < 1) ? '' : hour + ':';
    var min = (seconds / 60 % 60) << 0;
    min < 10 ? min = '0' + min : min;
    var sec = (seconds % 60) << 0;
    sec < 10 ? sec = '0' + sec : sec;
    if (min == 0 && sec == 0) {
      return '00:00';
    }
    return hour + min + ':' + sec + '';
  };
}