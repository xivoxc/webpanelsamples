export default function zeroWhenEmpty() {
  return function (number) {
    if (number) {
      return number;
    } else {
      return 0;
    }
  };
}