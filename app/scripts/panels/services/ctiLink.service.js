export default function ctiLink() {
  var _username = '';
  var _password = '';
  var _hostAndPort = '';
  var _secured = false;

  var _initCtiLocale = function (username, password, hostAndPort, secured) {
    _username = username;
    _password = password;
    _hostAndPort = hostAndPort;
    _secured = secured;
    var _wsProto = window.location.protocol === "http:" ? 'ws://' : 'wss://';

    var wsurl = _wsProto + _hostAndPort + '/xuc/ctichannel?username=' + _username + '&amp;agentNumber=0&amp;password=' + _password;
    Cti.debugMsg = false;
    Cti.WebSocket.init(wsurl, _username, 0);
  };

  return {
    init: _initCtiLocale,
    reconnect: function () {
      _initCtiLocale(_username, _password, _hostAndPort, _secured);
    }
  };

}