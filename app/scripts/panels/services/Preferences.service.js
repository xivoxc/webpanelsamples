import _ from 'lodash';

export default function Preferences(XucGroup, $filter, $http) {
  var _queuesFiltered = [];
  var _agentsFiltered = [];
  var _groupsFiltered = [];
  var _panelColor = 'blue';
  var _title = 'FILES D’ATTENTE DE TRANSFERT';
  var _titlePage = 'FILES D’ATTENTE DE TRANSFERT';
  var _groupDefinitions = [];
  var _groupName = 'queuegroup';
  var _filterAgentState = 'connected';
  var _selectedQueueName = '';
  var _thresholdStyle = {};
  var _charts = {};
  var _chartAgentState ={};
  var _chartCallTime ={};
  var _chartPercentageAnswered ={};

  _thresholdStyle.name='default';
  _thresholdStyle.style = {};
  
  _thresholdStyle.style.PercentageAnsweredBefore15 = {l: {lev: 0,style: 'rep15sBad'}, m: {lev: 80,style: 'rep15sAverage'}, h: {lev: 90,style: 'rep15sGood' }};
  _thresholdStyle.style.PercentageAbandonnedAfter15 = {  l: {lev: 0,style: 'abn15sGood'}, m: {lev: 2,style: 'abn15sAverage'},h: {lev: 3,style: 'abn15sBad'}};
  _thresholdStyle.style.WaitingCalls = {l: {lev: 0,style: 'waitingGood'}, m: {lev: 1,style: 'waitingBad'},h: {lev: 3,style: 'waitingBad'}};
  _thresholdStyle.style.LongestWaitTime = {l: {lev: 0,style: 'waitingTimeGood'}, m: {lev: 1,style: 'waitingTimeBad'},h: {lev: 3,style: 'waitingTimeBad'}};
  _thresholdStyle.style.AvailableAgents = {l: {lev: 0,style: 'agtDispoBad'},m: {lev: 0,style: 'agtDispoAverage'},h: {lev: 3,style: 'agtDispoGood'} };
  

  _chartAgentState.labels=["Disponible", "En Conversation", "Non Loggué","Pause","WrapUp"];
  _chartAgentState.options={legend: {display: true}};
  _chartAgentState.data=[];
  _chartAgentState.colors=['#5cb85c','#a34719','#a3a3a3','#d9534f','#F7612F'];

  _chartCallTime.labels=["Appels Entrants", "Appels Sortants","Pause"];
  _chartCallTime.options={legend: {display: true}};
  _chartCallTime.data=[];
  _chartCallTime.colors=['#00ffff','#000080','#800000','#d9534f'];


  var _getChartColors = function (data) { 
    if (data[0] < 87 ) {
      return '#cf2824';
    }  else if (data[0] >= 87 && data[0] < 93) {
      return '#d3831b';
    } else { return '#008000'; }
  };

  
  _chartPercentageAnswered.labels=["Percentage answered", "total"];
 
  _chartPercentageAnswered.options= {

    animation: {
      duration: 0
    },

    legend: {
      display: false
    },
    tooltips: {
      enabled: false
    },
    circumference: Math.PI,
    rotation : Math.PI,
    cutoutPercentage : 70 
  };
  
  
  _charts.chartPercentageAnswered = _chartPercentageAnswered;
  _charts.chartAgentState = _chartAgentState;
  _charts.chartCallTime = _chartCallTime;

  var _setGroupName = function (name) {
    _groupName = name;
  };

  var _getQueuesFiltered = function () {
    return _queuesFiltered;
  };

  var _setQueuesFiltered = function (qs) {
    _queuesFiltered = _.isArray(qs) ? qs : [];
  };

  var _setSelectQueues = function () {
    $http.get('data/selectQueues.json?time='+Date.now()).then(function (response) {
      angular.forEach(response.data, function (queue) {
        if (queue.name === _selectedQueueName) {
          _queuesFiltered = _.isArray(queue.queues) ? queue.queues : [];
        }
      });
    });

  };

  var _queueFilter = function (q) {
    if (_queuesFiltered.length > 0) {
      return _queuesFiltered.indexOf(q.name) != -1;
    } else {
      return true;
    }
  };

  var _setAgentsFiltered = function (as) {
    _agentsFiltered = _.isArray(as) ? as : [];
  };

  var _agentFilter = function (a) {
    if (_groupsFiltered.length > 0) {
      if ($filter('filter')(XucGroup.getGroups(), function (g) {
        return _groupsFiltered.indexOf(g.name) != -1 && g.id == a.groupId;
      }).length == 0) {
        return false;
      }
    }

    if (_agentsFiltered.length > 0) {
      return _agentsFiltered.indexOf(a.phoneNb) != -1;
    } else {
      return a.state != 'loggedout';
    }
  };

  var _agentLoggedOnFilter = function (a) {
    switch (_filterAgentState) {
    case "all":
      return true;
    case "connected":
      return a.state != 'AgentLoggedOut';
    default:
      return a.state != 'AgentLoggedOut';
    }
  };

  var _setGroupsFiltered = function (as) {
    _groupsFiltered = _.isArray(as) ? as : [];
  };

  var _setPanelColor = function (color) {
    _panelColor = color;
  };
  var _getPanelColor = function () {
    return _panelColor;
  };

  var _setFilterAgentState = function (filterAgentState) {
    _filterAgentState = filterAgentState;
  };
  var _getFilterAgentState = function () {
    return _filterAgentState;
  };

  var _getTile = function () {
    return _title;
  };

  var _getTitlePage = function () {
    return _titlePage;
  };

  var _getGroupDefinitions = function (name) {
    return _groupDefinitions[name];
  };
  var _getQueueGroups = function () {
    if (angular.isDefined(_groupDefinitions[_groupName])) {
      return _groupDefinitions[_groupName].queues;
    } else {
      return [];
    }
  };
  var _getQueueGroupsTitle = function () {
    if (angular.isDefined(_groupDefinitions[_groupName])) {
      _title=_groupDefinitions[_groupName].title;
      _titlePage=_groupDefinitions[_groupName].titlePage;
      return _groupDefinitions[_groupName].title;
    } else {
      return [];
    }
  };
  var _init = function () {
    $http.get('data/queuegroups.json?time='+Date.now()).then(function (response) {
      angular.forEach(response.data, function (group) {
        _groupDefinitions[group.name] = group.group;
      });
    });
  };

  var _setSelectedQueueName = function (selectedQueueName) {
    _selectedQueueName = selectedQueueName;
  };

  var _getSelectedQueueName = function () {
    return _selectedQueueName;
  };

  var _getThresholdStyle = function () {
    return _thresholdStyle.style;
  };

  var _setThresholdStyle = function (thresholdStyleName) {
    $http.get('data/thresholdStyle.json?time='+Date.now()).then(function (response) {
      angular.forEach(response.data, function (threshold) {
        if (threshold.name === thresholdStyleName) {
          _thresholdStyle =  threshold;
        }
      });
    });
  };

  
  var _getCharts = function () {
    return _charts;
  };

  return {
    init: _init,
    setQueuesFiltered: _setQueuesFiltered,
    getQueuesFiltered: _getQueuesFiltered,
    queueFilter: _queueFilter,

    setSelectQueues: _setSelectQueues,

    setAgentsFiltered: _setAgentsFiltered,
    agentFilter: _agentFilter,
    agentLoggedOnFilter: _agentLoggedOnFilter,

    setGroupsFiltered: _setGroupsFiltered,

    setPanelColor: _setPanelColor,
    getPanelColor: _getPanelColor,

    setFilterAgentState: _setFilterAgentState,
    getFilterAgentState: _getFilterAgentState,
    
    getChartColors: _getChartColors,
    getTitle: _getTile,
    getTitlePage: _getTitlePage,
    setGroupName: _setGroupName,
    getGroupDefinitions: _getGroupDefinitions,
    getQueueGroups: _getQueueGroups,
    getQueueGroupsTitle: _getQueueGroupsTitle,

    setSelectedQueueName: _setSelectedQueueName,
    getSelectedQueueName: _getSelectedQueueName,

    getThresholdStyle: _getThresholdStyle,
    setThresholdStyle: _setThresholdStyle,

    getCharts: _getCharts,
  };
}
