export default function PanelQueueTotal() {

  var queueStats = {};
  queueStats.sum = {};
  queueStats.max = {};
  queueStats.global = {};
  queueStats.qos= {};

  var _razCalc = function () {

    queueStats.sum.TotalNumberCallsEntered = 0;
    queueStats.sum.TotalNumberCallsAnswered = 0;
    queueStats.sum.TotalNumberCallsAbandonned = 0;
    queueStats.sum.TotalNumberCallsClosed = 0;
    queueStats.sum.TotalNumberCallsTimeout = 0;
    queueStats.sum.WaitingCalls = 0;
    queueStats.max.LongestWaitTime = 0;
    queueStats.max.EWT = 0;
    
    angular.forEach(Object.keys(queueStats.sum), function (statName) {
      queueStats.sum[statName] = 0;
    });

    angular.forEach(Object.keys(queueStats.global), function (statName) {
      queueStats.global[statName] = 0;
    });
    angular.forEach(Object.keys(queueStats.qos), function (statName) {
      queueStats.qos[statName] = 0;
    });
  };

  var _getCalcStats = function () {
    return angular.copy(queueStats);
  };

  var _calculateSum = function (property, value) {
    if(angular.isDefined(queueStats.sum[property])) {
      queueStats.sum[property] += value;
    } else if (isTotalNumberCalls(property))
      queueStats.sum[property] = value ;
  };

  var _calculateMax = function (property, value) {
    if (typeof(queueStats.max[property]) !== 'undefined' && !isNaN(parseInt(value))) {
      if (value > queueStats.max[property])
        queueStats.max[property] = value;
    }
  };
  var _updateQueueTotals = function (queue) {
    angular.forEach(queue, function (value, property) {
      _calculateSum(property, value);
      _calculateMax(property, value);
    });
  };
  var _calculatePercentages = function () {
    if (queueStats.sum.TotalNumberCallsEntered > 0) {
      queueStats.global.PercentageAnswered = _percent(queueStats.sum.TotalNumberCallsAnswered, queueStats.sum.TotalNumberCallsEntered);
      queueStats.global.PercentageAbandonned = _percent(queueStats.sum.TotalNumberCallsAbandonned, queueStats.sum.TotalNumberCallsEntered);

      angular.forEach(Object.keys(queueStats.sum), function (statName) {
        if (statName.indexOf('NumberCallsAnsweredBefore') > -1 && !isNaN(statName.substr(statName.length-2))) {
          queueStats.qos['Before' + statName.substr(statName.length-2)] = _percent(queueStats.sum[statName], queueStats.sum.TotalNumberCallsEntered);
          queueStats.qos['AnsweredBefore' + statName.substr(statName.length-2)] = _percent(queueStats.sum[statName], queueStats.sum.TotalNumberCallsAnswered);
        } else if (statName.indexOf('TotalNumberCallsAbandonnedAfter') > -1 && !isNaN(statName.substr(statName.length-2))) {
          queueStats.global['PercentageAbandonnedAfter' + statName.substr(statName.length-2)] = _percent(queueStats.sum[statName], queueStats.sum.TotalNumberCallsEntered);
        }
      });
    }
  };

  var _percent = function (nom = 0, denom = 0) {
    var percentage = 0;
    if (denom > 0) {
      percentage = (nom / denom) * 100;
      if (percentage > 100) percentage = 100;
    }
    return percentage;
  };

  var isTotalNumberCalls = function (property){
    return property.indexOf('TotalNumberCalls') !== -1;
  };

  var _calculate = function (queues) {
    _razCalc();
    angular.forEach(queues, function (queue) {
      _updateQueueTotals(queue);
    });
    _calculatePercentages();
  };
  _razCalc();

  return {
    getCalcStats: _getCalcStats,
    calculate: _calculate
  };

}