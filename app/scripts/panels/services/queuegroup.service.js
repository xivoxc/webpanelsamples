(function () {
  'use strict';

  angular
    .module('panels')
    .factory('QueueGroup', QueueGroup);

  QueueGroup.$inject = ['Preferences', 'XucQueue', 'PanelQueueTotal', '$rootScope'];

  function QueueGroup(preferences, xucQueue, panelQueueTotal, $rootScope) {
    var _groups = [];
    var _groupsTitle='';
    
    const _getGroups = function () {
      _updateGroups();
      return _groups; 
    };

    const _getGroupsTitle = function () {
      return _groupsTitle;
    };
    const _updateGroups = function () {
      let _groupDefs = preferences.getQueueGroups('queuegroup');
      let _charts = preferences.getCharts();
      _groupsTitle = preferences.getQueueGroupsTitle();
      _groups = [];

      for(let g of _groupDefs) {        
        let queues = xucQueue.getQueueByIds(g.queueids);
        panelQueueTotal.calculate(queues);
        let totals = panelQueueTotal.getCalcStats();
        
        let grp = {
          "charts": angular.copy(_charts),
          "name": g.name,
          "queues": queues,
          "totals": totals
        };

        let percentage = (grp.totals.sum.TotalNumberCallsEntered > 0) ? 
        (grp.totals.sum.TotalNumberCallsAnswered/grp.totals.sum.TotalNumberCallsEntered)*100 : 
        0;
        let chartPercentageAnsweredData=[percentage, 100 - percentage];
        grp.charts.chartPercentageAnswered.data = chartPercentageAnsweredData;
        grp.charts.chartPercentageAnswered.colors = [preferences.getChartColors(chartPercentageAnsweredData), '#D3D3D3'];
        _groups.push(grp);
      }

      $rootScope.$broadcast('PercentagesUpdated');
    };

    const _init = function () {
      _updateGroups();
    };


    return {
      getGroups: _getGroups,
      getGroupsTitle: _getGroupsTitle,
      init: _init
    };

  }
})();