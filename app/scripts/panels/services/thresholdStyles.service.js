export default function thresholdStyles() {
  var defaultStyle = 'rep15sGood';

  var thresholdsDefined = {};
  var defaultThresholds = {};
  defaultThresholds.PercentageAnsweredBefore15 = {
    l: {lev: 0,style: 'rep15sBad'},
    m: {lev: 80,style: 'rep15sAverage'},
    h: {lev: 90,style: 'rep15sGood' }
  };
  defaultThresholds.PercentageAbandonnedAfter15 = {
    l: {lev: 0,style: 'abn15sGood'},
    m: {lev: 2,style: 'abn15sAverage'},
    h: {lev: 3,style: 'abn15sBad'}
  };
  defaultThresholds.WaitingCalls = {
    l: {lev: 0,style: 'waitingGood'},
    m: {lev: 1,style: 'waitingBad'},
    h: {lev: 3,style: 'waitingBad'}
  };
  defaultThresholds.LongestWaitTime = {
    l: {lev: 0,style: 'waitingTimeGood'},
    m: {lev: 1,style: 'waitingTimeBad'},
    h: {lev: 3,style: 'waitingTimeBad'}
  };
  defaultThresholds.AvailableAgents = {
    l: {lev: 0,style: 'agtDispoBad'},
    m: {lev: 0,style: 'agtDispoAverage'},
    h: {lev: 3,style: 'agtDispoGood'}
  };

  var plThresholds = {};
  plThresholds.PercentageAnsweredBefore15 = {
    l: {lev: 0,style: 'rep15sBad'},
    m: {lev: 80,style: 'rep15sAverage'},
    h: {lev: 90,style: 'rep15sGood'}
  };
  plThresholds.PercentageAbandonnedAfter15 = {
    l: {lev: 0,style: 'abn15sGood'},
    m: {lev: 2,style: 'abn15sAverage'},
    h: {lev: 3,style: 'abn15sBad'}
  };
  plThresholds.WaitingCalls = {
    l: {lev: 0,style: 'waitingGood'},
    m: {lev: 1,style: 'waitingBad'},
    h: {lev: 3,style: 'waitingBad'}
  };
  plThresholds.LongestWaitTime = {
    l: {lev: 0,style: 'waitingTimeGood'},
    m: {lev: 1,style: 'waitingTimeBad'},
    h: {lev: 3,style: 'waitingTimeBad'}
  };
  plThresholds.AvailableAgents = {
    l: {lev: 0,style: 'agtDispoBad'},
    m: {lev: 0,style: 'agtDispoAverage'},
    h: {lev: 3,style: 'agtDispoGood'}
  };

  thresholdsDefined['st-'] = plThresholds;
  thresholdsDefined['pl-'] = plThresholds;


  var thresholds = defaultThresholds;

  var calculateStyle = function (name, value) {
    if (typeof (value) === 'undefined') return '';
    if (isNaN(value)) return '';
    if (value > thresholds[name].h.lev) {
      return thresholds[name].h.style;
    }
    if (value > thresholds[name].m.lev) {
      return thresholds[name].m.style;
    }
    return thresholds[name].l.style;
  };
  var _getStyle = function (name, value) {
    if (typeof (thresholds[name]) !== 'undefined') {
      return calculateStyle(name, value);
    } else {
      return defaultStyle;
    }
  };
  var _useThreshold = function (name) {
    if (typeof (thresholdsDefined[name]) !== 'undefined') {
      thresholds = thresholdsDefined[name];
    }
  };

  var _useThresholdStyle = function (thresholdStyle) {
    thresholds = thresholdStyle;
  };

  

  return {
    getStyle: _getStyle,
    useThreshold: _useThreshold,
    useThresholdStyle: _useThresholdStyle
  };
}