  export default class ArrDispatcher {

    constructor($log) {
      this.$log = $log;
      this._dispatched = [];
      this._rec = 0;
    }

    _consoleAgents(agents) {
      this.$log.info('agents : ' + agents.length);
      angular.forEach(agents, function (agent, key) {
        this.$log.info(key + ' ' + agent.firstName + ' ' + agent.phoneNb);
      });
    }

    _initDispatch (nbLines) {
      this._dispatched = [];
      for (var i = 0; i < nbLines; i++) {
        this._dispatched.push([]);
      }
    }

    _getNbLines(nbOfElt, nbOfCol) {
      var nbLines = Math.floor((nbOfElt / nbOfCol));
      if ((nbOfElt % nbOfCol) > 0) {
        nbLines += 1;
      }
      return nbLines;
    }

    _doDispatch(arr, nbOfCol) {
      this._rec = this._rec + 1;
      if (this._rec > 10000) {
        this.$log.error(`maximum recursive ${this._rec} attempt to dispatch agents nb of col : ${nbOfCol}`);
        this._consoleAgents(arr);
        return;
      }
      if (arr.length === 0) return;

      var nbLines = this._getNbLines(arr.length, nbOfCol);
      for (var i = 0; i < nbLines; i++) {
        this._dispatched[i].push(arr[i]);
      }
      arr.splice(0, nbLines);
      this._doDispatch(arr, nbOfCol - 1);
    }

    dispatch(arr, nbOfCol) {
      this._rec = 0;
      var nbLines = this._getNbLines(arr.length, nbOfCol);
      this._initDispatch(nbLines);

      this._doDispatch(arr, nbOfCol);
      return this._dispatched;
    }
  }
