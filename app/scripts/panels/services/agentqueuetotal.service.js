(function () {
  'use strict';

  angular
    .module('panels')
    .factory('AgentQueueTotal', AgentQueueTotal);

  AgentQueueTotal.$inject = ['$filter'];
 

  function AgentQueueTotal($filter) {

    var _zeroIfUndefined = function(value) {
      if(value) {
        return value;
      } else {
        return 0;
      }
    };
    
    var _calculate = function (agents, chartInMn = false) {
      var onCallStates = ['AgentOnCall', 'AgentOnAcdCall', 'AgentOnOutgoingCall', 'AgentOnIncomingCall'];
      var totals = {};
      totals.sum = {};
      totals.total = {};
      totals.sum.AgentsOnCall = 0;
      totals.sum.AgentsAvailable = 0;
      totals.sum.AgentsLoggedOn = 0;
      totals.sum.AgentLoggedOut = 0;
      totals.sum.AgentsOnPause = 0;
      totals.sum.AgentOnWrapup = 0;
      totals.sum.AgentsOnPauseWithCause = [];
      totals.sum.OutgoingCalls = 0;
      totals.total.ReadyTime = 0;
      totals.total.PausedTime = 0;
      totals.total.InbAcdCallTime = 0;
      totals.total.InbCallTime = 0;
      totals.total.OutCallTime = 0;


      for(let agent of agents) {
        if (chartInMn) {
          totals.total.PausedTime = $filter('number')(parseFloat(totals.total.PausedTime) + parseFloat(_zeroIfUndefined(agent.stats.PausedTime))/60, 0);
          
          totals.total.InbCallTime = $filter('number')(parseFloat(totals.total.InbCallTime) + parseFloat(_zeroIfUndefined(agent.stats.InbCallTime))/60, 0); 
          
          totals.total.OutCallTime = $filter('number')(parseFloat(totals.total.OutCallTime) + parseFloat(_zeroIfUndefined(agent.stats.OutCallTime))/60, 0);
        }  
        else {
          totals.sum.OutgoingCalls =totals.sum.OutgoingCalls+ _zeroIfUndefined(agent.stats.OutCalls);
          totals.total.ReadyTime = totals.total.ReadyTime + _zeroIfUndefined(agent.stats.ReadyTime);
          totals.total.PausedTime = totals.total.PausedTime + _zeroIfUndefined(agent.stats.PausedTime);
          totals.total.InbAcdCallTime = totals.total.InbAcdCallTime + _zeroIfUndefined(agent.stats.InbAcdCallTime);
          totals.total.InbCallTime = totals.total.InbCallTime + _zeroIfUndefined(agent.stats.InbCallTime);
          totals.total.OutCallTime = totals.total.OutCallTime + _zeroIfUndefined(agent.stats.OutCallTime);
        }  
        if (onCallStates.indexOf(agent.state) >= 0) {
          totals.sum.AgentsOnCall = totals.sum.AgentsOnCall + 1;
        }
        if (agent.state === 'AgentReady') {
          totals.sum.AgentsAvailable = totals.sum.AgentsAvailable + 1;
        }
        if (agent.state !== 'AgentLoggedOut') {
          totals.sum.AgentsLoggedOn = totals.sum.AgentsLoggedOn + 1;
        }
        if (agent.state == 'AgentLoggedOut') {
          totals.sum.AgentLoggedOut = totals.sum.AgentLoggedOut + 1;
        }
        if (agent.state == 'AgentOnWrapup') {
          totals.sum.AgentOnWrapup = totals.sum.AgentOnWrapup + 1;
        }
        
        if (agent.state === 'AgentOnPause') {
          totals.sum.AgentsOnPause = totals.sum.AgentsOnPause + 1;
          if(typeof totals.sum.AgentsOnPauseWithCause[agent.stateName] === 'undefined'){
            totals.sum.AgentsOnPauseWithCause[agent.stateName]=1;
          }else{
            totals.sum.AgentsOnPauseWithCause[agent.stateName]=totals.sum.AgentsOnPauseWithCause[agent.stateName]+1;
          }
        }
      }

      return totals;

    };
    
    return {
      calculate: _calculate
    };

  }
})();
