export default class AgentGroup {

  constructor(Preferences, XucAgent, $filter, AgentQueueTotal) {
    this.preferences = Preferences;
    this.xucAgent = XucAgent;
    this.$filter = $filter;
    this.agentQueueTotal = AgentQueueTotal;
    this._groups = [];
  }
  
  getGroups (chartInMn = false) {
    const _groupDefs = this.preferences.getQueueGroups('queuegroup');
    let _charts = this.preferences.getCharts();
    this._groups = [];
    for(let g of _groupDefs) {
      const agents = this.xucAgent.getAgentsInQueueByIds(g.queueids);
      const totals = this.agentQueueTotal.calculate(agents, chartInMn);
      const chartAgentStateData=[totals.sum.AgentsAvailable, totals.sum.AgentsOnCall,totals.sum.AgentLoggedOut,totals.sum.AgentsOnPause,totals.sum.AgentOnWrapup];
      const chartCallTimeData=[totals.total.InbCallTime,totals.total.OutCallTime,totals.total.PausedTime];
      _charts.chartAgentState.data=chartAgentStateData;
      _charts.chartCallTime.data=chartCallTimeData;
      const grp = {
        "name": g.name,
        "agents": agents,
        "totals": totals,
        "charts": _charts,
        "defs": g
      };
      this._groups.push(grp);
    }
    return this._groups;
  }

}
