import moment from 'moment';

(function () {
  'use strict';

  angular
    .module('panels')
    .controller('ctiLink', ctiLink);

  ctiLink.$inject = ['$rootScope', '$scope', '$timeout', '$location', 'ctiLink', 'Preferences', 'XucGroup', '$log'];

  function ctiLink($rootScope, $scope, $timeout, $location, ctiLink, Preferences, XucGroup, $log) {
    $scope.reconnectTimeout = 20000;
    $scope.clockFrequency = 5;
    $scope.connected = false;
    $scope.currentDay = '--/--/----';
    $scope.clock = '--:--:--';

    $scope.filteredd = Preferences.queuesFiltered;

    $rootScope.$on('$locationChangeSuccess', function () {
      var queues = $location.search().filter;
      if (queues) {
        Preferences.setQueuesFiltered(queues.split(','));
      }
      var agents = $location.search().agents;
      if (agents) {
        Preferences.setAgentsFiltered(agents.split(','));
      }
      var groups = $location.search().groups;
      if (groups) {
        Preferences.setGroupsFiltered(groups.split(','));
      }
      var panelColor = $location.search().panelColor;
      if (panelColor) {
        Preferences.setPanelColor(panelColor);
      }
      var groupName = $location.search().groupName;
      if (groupName) {
        Preferences.setGroupName(groupName);
      }
      var filterAgentState = $location.search().filterAgentState;
      if (filterAgentState) {
        Preferences.setFilterAgentState(filterAgentState);
      }
      var thresholdStyle = $location.search().thresholdStyle;
      if (thresholdStyle) {
        Preferences.setThresholdStyle(thresholdStyle);
      } else {
        Preferences.setThresholdStyle('default');
      }
      
      var selectQueues = $location.search().selectQueues;
      if (selectQueues) {
        Preferences.setSelectedQueueName(selectQueues);
      }

      var selectedLogo = $location.search().selectedLogo;
      if (selectedLogo) {
        $scope.selectedLogo = `/webpanels/images/sogetrel/${selectedLogo}`;
      } 
    });

    $scope.loggedOnHandler = function () {
      $rootScope.$broadcast('ctiLoggedOn');
      XucGroup.start();
    };

    $scope.linkStatusHandler = function (event) {
      $log.info('link status : ' + event.status);
      $scope.status = event.status;
      if (event.status !== 'opened') {
        $rootScope.$broadcast('linkDisConnected');
        $timeout($scope.reconnect, $scope.reconnectTimeout);
        $scope.connected = false;
      } else {
        $scope.connected = true;
      }
      $scope.$digest();
    };

    $scope.reconnect = function () {
      $log.info('trying to reconnect');
      ctiLink.reconnect();
    };

    // timeout
    $scope.updateClock = function () {
      $scope.currentDay = moment().format('DD/MM/YYYY');
      $scope.clock = moment().format('HH:mm:ss');
      $timeout($scope.updateClock, $scope.clockFrequency * 1000);
    };
    $timeout($scope.updateClock, $scope.clockFrequency * 1000);

    Cti.setHandler(Cti.MessageType.LOGGEDON, $scope.loggedOnHandler);
    Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, $scope.linkStatusHandler);

    $scope.updateTitle = function () {
      $scope.title = Preferences.getTitle();
      $scope.titlePage = Preferences.getTitlePage();
    };
    $timeout($scope.updateTitle, $scope.clockFrequency * 1000);
  }
})();