/* jshint ignore:start */
(function () {
  'use strict';

  angular
    .module('panels')
    .controller('queueTotal', queueTotal);

  queueTotal.$inject = ['XucQueue', '$scope', 'thresholdStyles'];

  function queueTotal(xucQueue, $scope, thresholdStyles) {
    $scope.totals = {};
    $scope.totals.classes = {};
    $scope.maxLongestWaitTime = 0;
    thresholdStyles.useThreshold($scope.queueFilter);

    var initTotals = function () {
      $scope.totals.TotalNumberCallsEntered = 0;
      $scope.totals.TotalNumberCallsAnswered = 0;
      $scope.totals.TotalNumberCallsAbandonned = 0;
      $scope.totals.TotalNumberCallsClosed = 0;
      $scope.totals.WaitingCalls = 0;
      $scope.totals.TotalNumberCallsAnsweredBefore15 = 0;
      $scope.totals.TotalNumberCallsAbandonnedAfter15 = 0;
      $scope.totals.GlobalPercentageAnsweredBefore15 = 0;
      $scope.totals.GlobalPercentageAbandonnedAfter15 = 0;

      $scope.totals.GlobalPercentageAbandonned = 0;

      $scope.totals.TotalNumberCallsEnteredLastHour = 0;
      $scope.totals.TotalNumberCallsAnsweredLastHour = 0;
      $scope.totals.TotalNumberCallsTimeout = 0;
      $scope.totals.GlobalPercentageAnswered = 0;
      $scope.totals.GlobalPercentageAnsweredLastHour = 0;

      $scope.totals.AvailableAgents = 0;
      $scope.totals.TalkingAgents = 0;

      $scope.maxLongestWaitTime = 0;
    };

    $scope.getStyle = function (name, value) {
      return thresholdStyles.getStyle(name, value);
    };

    var updateTotals = function (queue) {
      angular.forEach(queue, function (value, property) {
        if (typeof ($scope.totals[property]) !== 'undefined') {
          $scope.totals[property] = $scope.totals[property] + value;
        }
        if (property === 'LongestWaitTime' && value !== '-') {
          if (value > $scope.maxLongestWaitTime)
            $scope.maxLongestWaitTime = value;
        }
      });
    };

    var calculatePercentages = function () {
      if ($scope.totals.TotalNumberCallsEntered > 0) {
        $scope.totals.GlobalPercentageAnsweredBefore15 = ($scope.totals.TotalNumberCallsAnsweredBefore15 / $scope.totals.TotalNumberCallsEntered) * 100;
        $scope.totals.GlobalPercentageAbandonnedAfter15 = ($scope.totals.TotalNumberCallsAbandonnedAfter15 / $scope.totals.TotalNumberCallsEntered) * 100;

        $scope.totals.GlobalPercentageAnswered = ($scope.totals.TotalNumberCallsAnswered / $scope.totals.TotalNumberCallsEntered) * 100;
        $scope.totals.GlobalPercentageAnsweredLastHour = ($scope.totals.TotalNumberCallsAnsweredLastHour / $scope.totals.TotalNumberCallsEnteredLastHour) * 100;

        $scope.totals.GlobalPercentageAbandonned = ($scope.totals.TotalNumberCallsAbandonned / $scope.totals.TotalNumberCallsEntered) * 100;
      }
    };
    $scope.bravo = function () {
      return Math.round($scope.totals.GlobalPercentageAnswered) >= 90;
    };
    $scope.$watch('queues', function () {
      initTotals();
      angular.forEach($scope.queues, function (queue) {
        updateTotals(queue);
      });
      calculatePercentages();
    }, true);

    initTotals();
    calculatePercentages();
  }
})();