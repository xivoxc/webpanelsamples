(function () {
  'use strict';

  angular
    .module('panels')
    .controller('queueGroupController', queueGroupController)
    .filter('split', function () {
      return function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ');
      };
    });
    


    

  queueGroupController.$inject = ['$scope', 'Preferences', '$timeout', 'QueueGroup', 'XucQueue', 'AgentGroup', '$log', 'thresholdStyles'];

  function queueGroupController($scope, preferences, $timeout, queueGroup, xucQueue, agentGroup, $log,thresholdStyles) {
    var updateQueueFrequency = 5;

    setInterval(_updateGroups, 5000);

    $scope.title = preferences.getTitle();
    $scope.groupsTitle=queueGroup.getGroupsTitle();
    $scope.groups = queueGroup.getGroups();
    $scope.agentGroups = agentGroup.getGroups();
    $scope.agentGroupsInMin = agentGroup.getGroups(true);
    var defaultThresholds = {};
    defaultThresholds.PercentageAnsweredBefore15 = {
      l: {lev: 0,style: 'rep15sBad'},
      m: {lev: 80,style: 'rep15sAverage'},
      h: {lev: 90,style: 'rep15sGood' }
    };
    defaultThresholds.PercentageAbandonnedAfter15 = {
      l: {lev: 0,style: 'abn15sGood'},
      m: {lev: 2,style: 'abn15sAverage'},
      h: {lev: 3,style: 'abn15sBad'}
    };
    defaultThresholds.WaitingCalls = {
      l: {lev: 0,style: 'waitingGood'},
      m: {lev: 1,style: 'waitingBad'},
      h: {lev: 3,style: 'waitingBad'}
    };
    defaultThresholds.LongestWaitTime = {
      l: {lev: 0,style: 'waitingTimeGood'},
      m: {lev: 1,style: 'waitingTimeBad'},
      h: {lev: 3,style: 'waitingTimeBad'}
    };
    defaultThresholds.AvailableAgents = {
      l: {lev: 0,style: 'agtDispoBad'},
      m: {lev: 0,style: 'agtDispoAverage'},
      h: {lev: 3,style: 'agtDispoGood'}
    };
    
    var updateTotalQueueLonguestWaitTime = function () {
      angular.forEach($scope.groups, function (grp) {
        if (grp.totals.max.LongestWaitTime >= 0 && grp.totals.sum.WaitingCalls !== 0) {
          grp.totals.max.LongestWaitTime += updateQueueFrequency;
        } else {
          grp.totals.max.LongestWaitTime = 0;
        }
        $log.info(grp.totals.max.LongestWaitTime);
      });
      //$timeout(updateTotalQueueLonguestWaitTime,updateQueueFrequency*1000,false);
    };

    $timeout(updateTotalQueueLonguestWaitTime, 1000, false);

    var _getGroups = function () {
      $scope.groups = queueGroup.getGroups();
      $scope.agentGroups = agentGroup.getGroups();
      $scope.agentGroupsInMin = agentGroup.getGroups(true);
      $timeout(_getGroups, 2000, false);
    };

    $timeout(_getGroups, 1000, false);

    var _updateGroups = function () {
      $scope.groups = queueGroup.getGroups();
      $scope.agentGroups = agentGroup.getGroups();
      $scope.agentGroupsInMin = agentGroup.getGroups(true);
      $scope.groupsTitle = queueGroup.getGroupsTitle();
      $scope.$digest();
    };

    $scope.$on('ctiLoggedOn', function () {
      $scope.panelColor = preferences.getPanelColor();
      xucQueue.start();
      preferences.init();
      $scope.title = preferences.getTitle();
      $scope.groups = queueGroup.getGroups();
      $scope.agentGroups = agentGroup.getGroups();
      $scope.agentGroupsInMin = agentGroup.getGroups(true);
      thresholdStyles.useThresholdStyle(preferences.getThresholdStyle());
      
    });
    $scope.$on('QueueMemberUpdated', function () {
      $log.info('QueueMemberUpdated');
    });

    $scope.$on('AgentsLoaded', function () {
      $log.info('AgentsLoaded');
    });

    $scope.$on('AgentStateUpdated', function () {
      $log.info('AgentStateUpdated');
    });
    $scope.$on('QueueStatsUpdated', function () {
      $log.info('QueueStatsUpdated');
    });
    $scope.$on('PercentagesUpdated', function() {
      if (!$scope.$$phase) $scope.$apply();
    });

    $scope.agentPriorityInGroup = function (group) {
      return function (agent) {
        var minPrio = 99;
        angular.forEach(agent.queueMembers, function (value, key) {
          if (angular.isDefined(key) &&
            angular.isDefined(value) &&
            group.defs.queueids.indexOf(key) >= 0 &&
            minPrio > value)
            minPrio = value;
        });

        return minPrio;
      };
    };

    $scope.getAgentSortByPriority = function (group) {
      var res = [];
      angular.forEach(group.agents, function (agent) {
        var agentPriority = $scope.agentPriorityInGroup(group)(agent);
        if (res.length === 0) {
          res.push({
            priority: agentPriority,
            agents: [agent]
          });
        } else {
          var insered = false;
          angular.forEach(res, function (value) {
            if (value.priority === agentPriority) {
              value.agents.push(agent);
              insered = true;
            }
          });
          if (!insered) {
            res.push({
              priority: agentPriority,
              agents: [agent]
            });
          }
        }
      });
      return res;
    };

    $scope.getStyle = function (name, value) {
      return thresholdStyles.getStyle(name, value);
    };

    $scope.bravo = function (value,Threshold) {
      if(Math.round(value) >= Threshold){
        return true;
      }else{
        return false;
      }
    };
    
  }
})();