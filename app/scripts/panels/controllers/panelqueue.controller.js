/* jshint ignore:start */
const $ = require("jquery");
(function () {
  'use strict';

  angular
    .module('panels')
    .controller('panelQueueController', queue);

  queue.$inject = ['XucQueue', '$scope', '$rootScope', 'XucQueueTotal', '$timeout', '$filter', '$location', 'Preferences', '$log', 'thresholdStyles'];

  function queue(xucQueue, $scope, $rootScope, xucQueueTotal, $timeout, $filter, $location, Preferences, $log, thresholdStyles) {
    var updateQueueFrequency = 5;

    $scope.queueIds = []; // lfm add queue ids
    $rootScope.selectedQueues = [];

    $scope.panelColor = Preferences.getPanelColor();

    $rootScope.queues = [];
    $scope.stats = xucQueueTotal.getCalcStats();
    let initSelect = true;
    $scope.getQueues = function () {
      if ($scope.queueIds.length > 0) {
        $rootScope.queues = xucQueue.getQueueByIds($scope.queueIds);
      } else {
        $rootScope.queues = $filter('filter')(xucQueue.getQueues(), Preferences.queueFilter);
      }
      xucQueueTotal.calculate($rootScope.queues);
      $timeout($scope.getQueues, updateQueueFrequency * 1000, false);
      $scope.$digest();

      let selector = 'select[name=QueueSelect]';
      if (initSelect === true) {
        $.fn.selectpicker.Constructor.BootstrapVersion = '3';
        angular.element(document).ready(function () {
          $(selector).selectpicker('destroy');
          $(selector).selectpicker('render');
          $(selector).on('changed.bs.select', function () {
            $('select[name=AgentSelect]').selectpicker('refresh');
          });

        });
        initSelect = false;
      }
      $(selector).selectpicker('refresh');

    };
    $timeout($scope.getQueues, 1000, false);


    $scope.$on('ctiLoggedOn', function () {
      $scope.panelColor = Preferences.getPanelColor();
      xucQueue.start();
      thresholdStyles.useThresholdStyle(Preferences.getThresholdStyle());
    });

    $scope.$on('linkDisConnected', function () {
      $log.info('queueController linkDisConnected');
      $rootScope.queues = [];
    });

    $scope.atLeastOneWaitingCall = function () {
      return $scope.stats.sum.WaitingCalls > 0;
    };


    $scope.getQueuesFiltered = (selectedQueues) => {
      if (selectedQueues && selectedQueues.length !== 0) {
        return $scope.queues.filter(elem =>
          selectedQueues.indexOf(elem.id.toString()) !== -1
        );
      }
      return $scope.queues;
    };

    $scope.getThresholdStyles = (name, value) => {
      return thresholdStyles.getStyle(name, value);
    };  
  }
})();