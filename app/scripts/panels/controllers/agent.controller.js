import moment from 'moment';
import _ from 'lodash';


(function () {
  'use strict';

  angular
    .module('panels')
    .controller('agent', agent);

  agent.$inject = ['XucAgent', '$scope', '$timeout', 'ArrDispatcher', '$filter', 'Preferences', 'thresholdStyles'];

  function agent(xucAgent, $scope, $timeout, arrDispatcher, $filter, Preferences, thresholdStyles) {

    let $ = require('jquery');

    $scope.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true
    };
    $scope.selectedAgents = [];

    var agentStateTimer = 2;
    var agentStateWarningThreshold = 240;
    var agentStateWarningStyle = 'AgentStateWarning';
    var nbOfColumns;
    $scope.agentsLoggedInQueues = [];

    if ($scope.queueIds && $scope.queueIds.length > 0) { // lfm
      nbOfColumns = 3;
      $scope.agents = xucAgent.getAgentsInQueueByIds($scope.queueIds);
    } else {
      nbOfColumns = 4;
      $scope.agents = [];
    }
    $scope.agentsInCol = [];
    $scope.agentsInQueues = [];

    $scope.$on('ctiLoggedOn', function () {
      xucAgent.start();
      thresholdStyles.useThresholdStyle(Preferences.getThresholdStyle());
    });

    var onAgentsUpdated = _.throttle(function () {
      if ($scope.queueIds && $scope.queueIds.length > 0) { // lfm
        $scope.agents = xucAgent.getAgentsInQueueByIds($scope.queueIds);
      } else {
        $scope.agents = xucAgent.getAgents();
        $scope.agentsInQueues = xucAgent.getAgents();
        $scope.agents = $filter('filter')($scope.agents, Preferences.agentFilter);
      }
      $scope.agents = $filter('filter')($scope.agents, Preferences.agentLoggedOnFilter);
      $scope.agents = $filter('orderBy')($scope.agents, 'firstName', false);
      _.each($scope.agents, function (agent) {
        agent.name = agent.firstName + ' ' + agent.lastName;
      });
      $scope.agentsInCol = arrDispatcher.dispatch(angular.copy($scope.agents), nbOfColumns);
      $scope.$digest();
      $scope.resetView();

    }, 500);

    $scope.agentInQueue = function (queueId) {
      return $filter('filter')(xucAgent.getAgentsInQueueByIds([queueId]), Preferences.agentLoggedOnFilter);
    };


    $scope.getAgentsFilteredByQueues = (selectedQueues) => {
      return selectedQueues && selectedQueues.length > 0 ? $filter('orderBy')($filter('filter')(xucAgent.getAgentsInQueueByIds(selectedQueues), Preferences.agentLoggedOnFilter) , 'firstName', false): $scope.agents;
    };
    $scope.getAgentsFiltered = (selectedAgents, selectedQueues) => {
      if (selectedAgents && selectedAgents.length !== 0) {
        return $scope.getAgentsFilteredByQueues(selectedQueues).filter(elem =>
          selectedAgents.indexOf(elem.id.toString()) !== -1
        );
      }
      return $scope.getAgentsFilteredByQueues(selectedQueues);
    };
    let initSelect = true;
    $scope.resetView = () => {
      let selector = 'select[name=AgentSelect]';
      if (initSelect) {
        $.fn.selectpicker.Constructor.BootstrapVersion = '3';
        angular.element(document).ready(function () {
          $(selector).selectpicker('destroy');
          $(selector).selectpicker('render');
        });
        initSelect = false;
      }
      $(selector).selectpicker('refresh');
    };

    $scope.getLeftTable = (selectedAgents, selectedQueues) => {
      let list = $scope.getAgentsFiltered(selectedAgents, selectedQueues);
      if (list && list.length > 1) {
        return list.slice(0, Math.ceil(list.length / 2));
      }
      return list;
    };
    $scope.getRightTable = (selectedAgents, selectedQueues) => {
      let list = $scope.getAgentsFiltered(selectedAgents, selectedQueues);
      if (list && list.length > 1) {
        return list.slice(Math.ceil(list.length / 2));
      }
      return [];
    };
    $scope.$on('QueueMemberUpdated', function () {
      onAgentsUpdated();
    });

    $scope.$on('AgentsLoaded', function () {
      onAgentsUpdated();
    });

    $scope.$on('AgentStateUpdated', function () {
      onAgentsUpdated();
    });

    $scope.$on('linkDisConnected', function () {
      $scope.agents.length = 0;
      $scope.agentsInCol.length = 0;
    });

    var updateAgentState = function (agent) {
      if (agent.state !== 'AgentLoggedOut') {
        agent.timeInState = moment().countdown(agent.momentStart);
      } else {
        agent.timeInState = moment().add('seconds', 0);
        agent.phoneNb = '';
      }
    };
    $scope.updateAgentStates = function () {
      angular.forEach($scope.agentsInCol, function (line) {
        angular.forEach(line, function (agent) {
          updateAgentState(agent);
        });
      });
      $scope.$digest();
      $timeout($scope.updateAgentStates, agentStateTimer * 1000, false);
    };
    $timeout($scope.updateAgentStates, 1000, false);

    $scope.getStateStyle = function (stateTimer) {
      if (stateTimer && stateTimer.value) {
        if (Math.abs(stateTimer.value) / 1000 > agentStateWarningThreshold) {
          return agentStateWarningStyle;
        }
      }
    };

    $scope.getStylesForCountdown = function (agent) {
      return $scope.getStyle(agent.state == 'AgentOnPause' ? 'ccSupervisionAgentOnPause' : 'ccSupervisionAgentOnAcdCall', $scope.timeInSeconds(agent));
    };

    $scope.timeInSeconds = function (agent) {
      return agent.timeInState.seconds + agent.timeInState.minutes * 60 + agent.timeInState.hours * 3600;
    };

    $scope.getClassesForPauseAndAcdTime = function (agent) {
      if (agent.state != undefined && (agent.state == 'AgentOnPause' || agent.state == 'AgentOnAcdCall')) {
        return $scope.getStylesForCountdown(agent);
      } else {
        return '';
      }
    };

    $scope.getStyle = function (name, value) {
      return thresholdStyles.getStyle(name, value);
    };
  }
})();
