import angular from 'angular';
import Preferences from './services/Preferences.service';
import ctiLink from './services/ctiLink.service';
import ArrDispatcher from './services/ArrDispatcher.service';
import AgentGroup from './services/agentgroup.service';
import thresholdStyles from './services/thresholdStyles.service';
import PanelQueueTotal from './services/panelQueueTotal.service';
import secondFormater from './filters/secondFormater.filter';
import zeroWhenEmpty from './filters/zeroWhenEmpty.filter';
import 'font-awesome/css/font-awesome.min.css';

var panelsModule = angular.module('panels', ['xcCti', 'pascalprecht.translate'])
  .config(function ($locationProvider) {
    $locationProvider.html5Mode(true);
  });

panelsModule.factory('Preferences', Preferences)
  .factory('ctiLink', ctiLink)
  .factory('thresholdStyles', thresholdStyles)
  .factory('PanelQueueTotal', PanelQueueTotal)
  .service('ArrDispatcher', ArrDispatcher)
  .service('AgentGroup', AgentGroup);

panelsModule
  .filter('secondFormater', secondFormater)
  .filter('zeroWhenEmpty', zeroWhenEmpty);

panelsModule.config(function ($translateProvider) {
  $translateProvider.translations('fr', {
    AgentReady: 'Prêt',
    AgentOnPause: 'En pause',
    AgentLoggedOut: 'Deloggué',
    AgentOnCall: 'Conv.',
    AgentOnIncomingCall: 'Conv. Entr.',
    AgentOnOutgoingCall: 'Conv. Sort.',
    AgentOnWrapup: 'Post Appel',
    AgentOnAcdCall: 'Conv. ACD',
    AgentDialing: 'Num.',
    AgentRinging: 'Sonnerie',
    Fax: 'Fax',
    NoAnswer: 'Non réponse',
    Answered: 'Répondu',
    Callback: 'A rappeler'
  });
  $translateProvider.translations('en', {
    AgentReady: 'Ready',
    AgentOnPause: 'Paused',
    AgentLoggedOut: 'Loggued Out',
    AgentOnCall: 'On Call',
    AgentOnIncomingCall: 'Inc. Call',
    AgentOnOutgoingCall: 'Out. Call',
    AgentOnWrapup: 'Wrapup',
    AgentOnAcdCall: 'ACD Call',
    AgentDialing: 'Dialing',
    AgentRinging: 'Ringing',
    Fax: 'Fax',
    NoAnswer: 'No answer',
    Answered: 'Answered',
    Callback: 'Call back'
  });
  $translateProvider.determinePreferredLanguage();
  $translateProvider.fallbackLanguage(['fr']);
});