import 'angular';

(function () {
  'use strict';

  angular
    .module('panels')
    .run(['$rootScope', 'XucLink', 'Preferences', '$log', '$http', function ($rootScope, XucLink, preferences, $log, $http) {
      var host = 'localhost';
      var port = '8090';
      var username = 'webpanels';
      var password = 'webpa';

      $http.get("./config/config.json").then(function (response) {
        host = response.data.host;
        port = response.data.port;
        username = response.data.username;
        password = response.data.password;

        var hostAndPort = host + ':' + port;
        $log.info('starting web panels ' + hostAndPort + ' whith user ' + username);
        XucLink.setHostAndPort(hostAndPort);
        XucLink.login(username, password, 0, false);
      }, function () {
        var hostAndPort = host + ':' + port;
        $log.info('starting web panels ' + hostAndPort + ' whith user ' + username);
        XucLink.setHostAndPort(hostAndPort);
        XucLink.login(username, password, 0, false);
      });

    }]);
})();
