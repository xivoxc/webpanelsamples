import 'angular';

import 'xccti/cti-webpack';

import './panels.module';

import './services/queuegroup.service';
import './services/agentqueuetotal.service';

import './controllers/ctiLink.controller';
import './controllers/queueTotal.controller';
import './controllers/queuegroup.controller';
import './controllers/agent.controller';
import './controllers/panelqueue.controller';

import './panels.run';

