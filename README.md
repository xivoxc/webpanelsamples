Setup for Development
=====================

* Get the dependencies and check tests
  ```
  npm install
  npm test
  npm run build
  ```

* Update connexion parameters in app/config/config.json

    ```json
    {
      "host" : "192.168.56.3",
      "port" : "8090",
      "username" : "webpanels",
      "password" : "webpa",
      "description" : "do not remove"
    }
    ```

* The username and password should correspond to a user that you have created in the webi. 
* You should choose another user than Xuc technical.    

* Start the server

  ```
  npm start
  ```

* If the xuc is not connecting after opening a webpanel, there might be a CORS issue. You should change the url from 0.0.0.0:8080 to localhost:8080. This helps the browser recognize that you are in dev mode so it won't block the connexion. 

Create a new webpanel
===================

* Create the html for the panel in the repertory app/
  * Add a tag `<base target="_blank" href=".">` in the head of the page.
  * include the webpack bundle js & css and the other includes you need.
    ```
    <link rel="stylesheet" type="text/css" href="panel_name.styles.css">
    <link rel="stylesheet" type="text/css" href="vendor.styles.css">
    <script src="panel_name.bundle.js"></script>
    ```
* Create the css file in the repertory app/style/panels.
* Create a new repertory in app/scripts with the name of the panel.
  * Create a file app.js inside.
  * In the app.js you have to import all the css & js file you need.
  * If you need to create specific angular module or controller, put it in this folder.
* In the webpack.config.js file :
  * You have to export the new app.js.
  * Create the alias.
  * Add the folder to the folder who are moved.

Build locally
=============

Build docker image :

* update TARGET_VERSION
* and start ./docker_build.sh

Usage
=====

Url parameters
--------------

* filter : filter queues to display
* groups : filter agents from groups
* agents : fitler specific agents
* panelColor : for perqueuelogo.html change panels colors and border can be green, blue, grey, pink, purple, orange
* xuc : xuc server host and port i.e : '129.12.1.1:8090'
* groupName : select group of queues that is use, from data/queuegroup.json
* selectQueues : select specifics queues from a group wich are specify in data/selectQueues.json
* filterAgentState=all : to see disconnected agent aswell

Examples
--------

    filter=queue1,queue2&groups=gr1&agents=358,362,379,382,380,395,353
    filter=queue1,queue2&agents=358,362,379,382,380
    filter=queue1,queue2&groups=gr2
    groupName=group

Display a panel : <http://localhost:8080/globaloptpanel.html?filter=blue>

perqueuelogo.html, summarylogo.html & Th1.html are using queue groups defined in queuegroups.json

* title is not used
* name is use as a title in perqueuelogo.html & Th1

  ```json
  [
    .....
    {
      "name" :"queuegroup",
      "group": {
        "title":"Transfert",
        "queues" :  [
            {"name": "Cars","queueids" : [1,7]},
            {"name": "Planes","queueids" : [171,11]}
        ]
      }
    },
    {
      "name" :"fs",
      "group": {
        "title":"Frais de Santé",
        "queues" :  [
            {"name": "Frais de Santé","queueids" : [1,7]}
        ]
      }
    }
    .....
  ]
  ```

Deploy Using Docker
===================

**Note: This is configuration for Electra. Documentation for Deneb is here:** 
https://gitlab.com/xivoxc/webpanelsamples/-/blob/2019.12/README.md#deploy-using-docker

Customize docker-xivocc.yml file:

* xuc host
* xuc port number
* username & password for the CTI user used for the XUC connection
* create a named volume to share data between the webpanels and nginx container
* see the Customization section below if you want to modify anything inside the named volume

```yaml
    version: "3.7"

    services:
      .....

      webpanels:
        image: xivoxc/webpanels:latest

        environment:
        - XUC_HOST=127.0.0.1
        - XUC_PORT=8090
        - XUC_USERNAME=usrwpa
        - XUC_PASSWORD=lskdu78

        volumes:
        - webpanels:/usr/share/webpanels/

      nginx:
        .....

        volumes:
        .....
        - webpanels:/usr/share/webpanels/
        - /etc/docker/compose/webpanels/queuegroups.json:/usr/share/webpanels/data/queuegroups.json
        - /etc/docker/compose/webpanels/thresholdStyle.json:/usr/share/webpanels/data/thresholdStyle.json
        .....

    volumes:
      webpanels:
```

Initialize the named volume with data
-------------------------------------

```bash
xivocc-dcomp stop nginx
xivocc-dcomp rm -f nginx webpanels
docker volume rm xivocc_webpanels
xivocc-dcomp up -d
```

Customize
---------

With the named volume you can't customize images or json files by using binds (as before Electra).
You can only overwrite them in the named volume:

* add the customized files to `/etc/docker/compose/webpanels/`
* you can then add more bindings such as `thresholdStyle.json`, `queuegroups.json` or any logo file to override inside mounted volume


Troubleshooting
===============
You may need to manually remove and recreate the webpanels volume after modification of external files 
or when deploying a new version, Docker currently has some issues related to this kind of operation.

When doing `docker ps -a` if the webpanel status is "Exited", try : 

```bash
xivocc-dcomp stop nginx && xivocc-dcomp rm -v nginx webpanels && xivocc-dcomp up -d
``` 

License
=======

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the COPYING and COPYING.LESSER files for details.
