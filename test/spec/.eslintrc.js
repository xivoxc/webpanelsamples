module.exports = {
  "env": {
    "jasmine": true
  },
  "globals": {
    "MockAgentBuilder": true,
    "MockGroupBuilder": true,
    "QueueBuilder": true,
    "customMatchers": true,
    "CallbackListBuilder": true,
    "CallbackRequestBuilder": true,
    "UserBuilder": true
  }
};