'use strict';

describe('Controller: agent', function () {

    // load the controller's module
    beforeEach(module('optpanel'));
    beforeEach(module('xuc.services'));

    var ctrl,
        scope,
        preferences,
        queueGroup,
        agentGroup;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope, _Preferences_) {

        scope = $rootScope.$new();
        preferences = _Preferences_;

        ctrl = $controller('queueGroupController', {
            $scope: scope,
            Preferences: preferences
        });
    }));

    it('can instanciate controller', function(){
        expect(ctrl).not.toBeUndefined();
    });



});
