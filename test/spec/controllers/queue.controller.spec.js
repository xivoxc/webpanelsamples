'use strict';

describe('Controller: queue', function () {

    // load the controller's module
  beforeEach(module('optpanel'));
  beforeEach(module('xuc.services'));

  var ctrl,
    scope,
    preferences,
    xucQueue,
    xucQueueTotal;

    // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, _XucQueue_, _XucQueueTotal_, _$filter_, _Preferences_) {

    scope = $rootScope.$new();
    preferences = _Preferences_;
    xucQueue = _XucQueue_;
    xucQueueTotal = _XucQueueTotal_;

    ctrl = $controller('queue', {
      $scope: scope,
      Preferences: preferences,
      XucQueue: xucQueue,
      XucQueueTotal: xucQueueTotal
    });
  }));

  it('can instanciate controller', function () {
    expect(ctrl).not.toBeUndefined();
  });

  it("return the collection sort by the order of the Preference queueFilter collection", function () {
    spyOn(xucQueue, 'getQueues');
    spyOn(preferences, 'queueFilter');
    expect(xucQueue.getQueues).toHaveBeenCalled();
  });
});