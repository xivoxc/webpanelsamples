'use strict';

describe('Controller: queueGroupController', function () {

    // load the controller's module
    beforeEach(module('optpanel'));
    beforeEach(module('xuc.services'));

    var ctrl,
        scope,
        preferences,
        queueGroup,
        agentGroup;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope, _Preferences_, _QueueGroup_, _AgentGroup_) {

        scope = $rootScope.$new();
        preferences = _Preferences_;
        queueGroup = _QueueGroup_;
        agentGroup = _AgentGroup_;
        spyOn(preferences,'getTitle');
        spyOn(queueGroup,'getGroups');
        spyOn(agentGroup,'getGroups');

        ctrl = $controller('queueGroupController', {
            $scope: scope,
            Preferences: preferences,
            QueueGroup : queueGroup,
            AgentGroup : agentGroup
        });
    }));

    it('can instanciate controller', function(){
        expect(ctrl).not.toBeUndefined();
    });


    it('should get title from preferences', function() {
        expect(preferences.getTitle).toHaveBeenCalled();
    });

    it('should get groups from queue group service and agent group services', function(){
        expect(queueGroup.getGroups).toHaveBeenCalled();
        expect(agentGroup.getGroups).toHaveBeenCalled();
    });

    it('can sort agent base on queue membership priority', function(){
        var group = {
            "name": "Run 1",
            "defs": {
                "queueids": [5]
            }
        };
        var agent1 = MockAgentBuilder(1,"James", "Bond").inQueue(5, 1).build();
        var agent2 = MockAgentBuilder(2,"James", "Bond").inQueue(5, 0).build();
        //agent1 > agent2
        var ag1order = scope.agentPriorityInGroup(group)(agent1);
        var ag2order = scope.agentPriorityInGroup(group)(agent2);
        console.log("Agent 1 order : " + ag1order);
        expect(ag1order > ag2order).toBe(true);
    });

    it('can sort agent base on queue membership priority on queues defined in current group', function(){
        var group = {
            "name": "Run 1",
            "defs": {
                "queueids": [2]
            }
        };
        var agent1 = MockAgentBuilder(1,"James", "Bond").inQueue(5, 0).inQueue(2, 1).build();
        var agent2 = MockAgentBuilder(2,"James", "Bond").inQueue(5, 1).inQueue(2, 0).build();

        // agent1 > agent2 only for queue 2 (as defined in group 'Run 1'
        var ag1order = scope.agentPriorityInGroup(group)(agent1);
        var ag2order = scope.agentPriorityInGroup(group)(agent2);
        expect(ag1order > ag2order).toBe(true);
    });

    it('can sort agent base on queue membership priority', function(){
        var group = {
            "name": "Run 1",
            "defs": {
                "queueids": [5, 7]
            }
        };
        var agent1 = MockAgentBuilder(1,"James", "Bond").inQueue(5, 1).inQueue(7, 1).build();
        var agent2 = MockAgentBuilder(2,"James", "Bond").inQueue(5, 1).inQueue(7, 0).build();

        // agent1 > agent2 for queue 2, queue 1 is ignore as it's same prio
        var ag1order = scope.agentPriorityInGroup(group)(agent1);
        var ag2order = scope.agentPriorityInGroup(group)(agent2);
        expect(ag1order > ag2order).toBe(true);
    });

    it('return object with agents sorted on queue membership priority',function () {
        var agent1 = MockAgentBuilder(1,"James", "Bond").inQueue(1, 0).build();
        var agent2 = MockAgentBuilder(2,"James", "Bond").inQueue(1, 1).build();
        var agent3 = MockAgentBuilder(3,"James", "Bond").inQueue(1, 1).build();
        var group = {
            "name": "RUN 1",
            "agents" : [
                agent1,
                agent2,
                agent3
            ],
            "defs": {
                "queueids": [1]
            }
        };
        var res = scope.getAgentSortByPriority(group);
        expect(res).toEqual(
            [
                {
                    priority: 0,
                    agents: [
                        agent1
                    ]
                },
                {
                    priority: 1,
                    agents: [
                        agent2,
                        agent3
                    ]
                }
            ]
        );
    });

    it('return object with agents sorted on queue membership priority only for queues and agents of the group',function () {
        var agent1 = MockAgentBuilder(1,"James", "Bond").inQueue(1, 0).inQueue(2, 1).build();
        var agent2 = MockAgentBuilder(2,"James", "Bond").inQueue(1, 1).inQueue(2, 0).build();
        var agent3 = MockAgentBuilder(3,"James", "Bond").inQueue(1, 1).inQueue(2, 0).build();

        var group = {
            "name": "RUN 1",
            "agents" : [
                agent1,
                agent2,
            ],
            "defs": {
                "queueids": [1]
            }
        };
        var res = scope.getAgentSortByPriority(group);

        expect(res).toEqual(
            [
                {
                    priority: 0,
                    agents: [
                        agent1
                    ]
                },
                {
                    priority: 1,
                    agents: [
                        agent2
                    ]
                }
            ]
        );
    });

});
