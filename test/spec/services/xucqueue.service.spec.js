'use strict';
describe('Xuc service', function() {
    var xucQueue;
    var $rootScope;

    var QueueBuilder = function(id,name) {
        this.queue = {};
        this.queue.name = name;
        this.queue.displayName = '';
        this.queue.id = id;
        this.stats = {};
        this.stats.queueId = id;
        this.stats.counters = [];

        this.withStat = function(statName,value) {
            var stat = {};
            stat.statName = statName;
            stat.value = value;
            this.stats.counters.push(stat);
            return this;
        };
        this.withDisplayName = function(displayName) {
            this.queue.displayName = displayName;
            return this;
        };
        this.build = function() {
            xucQueue.onQueueConfig(this.queue);
            xucQueue.onQueueStatistics(this.stats);
            return this.queue;
        };
        return this;
    };

    beforeEach(module('xuc.services'));


    beforeEach(inject(function(_XucQueue_,_$rootScope_) {
        xucQueue = _XucQueue_;
        $rootScope = _$rootScope_;
        spyOn($rootScope, '$broadcast');
    }));

    it('can instanciate service', function(){
        expect(xucQueue).not.toBeUndefined();
    });

    it('can return queues by a list of queue names', function(){
      var q1 = new QueueBuilder(101,'Sky').build();
      var q2 = new QueueBuilder(102,'Earth').build();
      var q3 = new QueueBuilder(103,'Sea').build();

      var queueNames = ['Sky', 'Sea'];
      var queues = xucQueue.getQueueByNames(queueNames);

      expect(queues.length).toBe(2);
      expect(queues).toContain(q1);
      expect(queues).toContain(q3);

    });

});
