'use strict';

describe('Service: PanelQueueTotal', function () {
  let panelQueueTotal;


  beforeEach(angular.mock.module('panels'));

  beforeEach(angular.mock.inject(function (_PanelQueueTotal_) {
    panelQueueTotal = _PanelQueueTotal_;
  }));

  it('can instanciate service', function () {
    expect(panelQueueTotal).not.toBeUndefined();
  });

  it('should reset all stat to 0', function () {
    const q1 = QueueBuilder('customer', 'queue customer')
      .withStat('WaitingCalls', 37)
      .withStat('TotalNumberCallsAnsweredBefore35', 5)
      .withStat('TotalNumberCallsEntered', 10)
      .build();
    const queues = [q1];

    panelQueueTotal.calculate(queues);
    panelQueueTotal.calculate(queues);

    const stats = panelQueueTotal.getCalcStats();

    expect(stats.sum.WaitingCalls).toBe(37);
    expect(stats.sum.TotalNumberCallsAnsweredBefore35).toBe(5);

  });

  it('should calculate sums', function () {
    const q1 = QueueBuilder('customer', 'queue customer')
      .withStat('WaitingCalls', 37)
      .withStat('TotalNumberCallsAnsweredBefore35', 5)
      .withStat('TotalNumberCallsEntered', 10)
      .build();
    const q2 = QueueBuilder('sell', 'queue sell')
      .withStat('WaitingCalls', 43)
      .withStat('TotalNumberCallsAnsweredBefore35', 15)
      .withStat('TotalNumberCallsEntered', 30)
      .build();

    const queues = [q1, q2];

    panelQueueTotal.calculate(queues);

    const stats = panelQueueTotal.getCalcStats();

    expect(stats.sum.WaitingCalls).toBe(80);
    expect(stats.sum.TotalNumberCallsAnsweredBefore35).toBe(20);

  });

  it('should calculate qos', function () {
    const q1 = QueueBuilder('customer', 'queue customer')
      .withStat('TotalNumberCallsAnsweredBefore35', 5)
      .withStat('TotalNumberCallsEntered', 12)
      .build();
    const q2 = QueueBuilder('sell', 'queue sell')
      .withStat('TotalNumberCallsAnsweredBefore35', 15)
      .withStat('TotalNumberCallsEntered', 28)
      .build();

    const queues = [q1, q2];

    panelQueueTotal.calculate(queues);

    const stats = panelQueueTotal.getCalcStats();

    expect(stats.qos.Before35).toBe(50.0);
  });

  it('should create only defined properties in the object', function () {
    const q1 = QueueBuilder('customer', 'queue customer')
      .withStat('id', 5)
      .withStat('displayName', 'Super Queue')
      .withStat('TotalNumberCallsEntered', 8)
      .withStat('TotalNumberCallsAnswered', 4)
      .withStat('TotalNumberCallsAbandonned', 3)
      .withStat('TotalNumberCallsClosed', 1)
      .withStat('TotalNumberCallsTimeout', 6)
      .withStat('WaitingCalls', 5)
      .withStat('TotalNumberCallsAnsweredBefore15', 2)
      .withStat('TotalNumberCallsAbandonnedAfter15', 4)
      .withStat('TotalNumberCallsAnsweredBefore20', 4)
      .withStat('TotalNumberCallsAbandonnedAfter20', 6)
      .withStat('LongestWaitTime', 12)
      .withStat('EWT', 11)
      .build();
    const q2 = QueueBuilder('sell', 'queue sell')
      .withStat('id', 5)
      .withStat('displayName', 'Super Queue 2')
      .withStat('TotalNumberCallsEntered', 6)
      .withStat('TotalNumberCallsAnswered', 3)
      .withStat('TotalNumberCallsAbandonned', 4)
      .withStat('TotalNumberCallsClosed', 1)
      .withStat('TotalNumberCallsTimeout', 6)
      .withStat('WaitingCalls', 4)
      .withStat('TotalNumberCallsAnsweredBefore15', 5)
      .withStat('TotalNumberCallsAbandonnedAfter15', 3)
      .withStat('TotalNumberCallsAnsweredBefore20', 10)
      .withStat('TotalNumberCallsAbandonnedAfter20', 8)
      .withStat('LongestWaitTime', 10)
      .withStat('EWT', 13)
      .build();

    const queues = [q1, q2];

    panelQueueTotal.calculate(queues);

    const stats = panelQueueTotal.getCalcStats();

    expect(stats).toEqual(
      {
        "sum": {
          "TotalNumberCallsEntered": 14,
          "TotalNumberCallsAnswered": 7,
          "TotalNumberCallsAbandonned": 7,
          "TotalNumberCallsClosed": 2,
          "TotalNumberCallsTimeout": 12,
          "WaitingCalls": 9,
          "TotalNumberCallsAnsweredBefore15": 7,
          "TotalNumberCallsAbandonnedAfter15": 7,
          "TotalNumberCallsAnsweredBefore20": 14,
          "TotalNumberCallsAbandonnedAfter20": 14
        },
        "max": {
          "LongestWaitTime": 12,
          "EWT": 13
        },
        "global": {
          "PercentageAnswered": 50,
          "PercentageAbandonned": 50,
          "PercentageAbandonnedAfter15": 50,
          "PercentageAbandonnedAfter20": 100
        },
        "qos" : {
          "Before15": 50,
          "Before20": 100,
          "AnsweredBefore15": 100,
          "AnsweredBefore20": 100
        }
      }
    );
  });
});