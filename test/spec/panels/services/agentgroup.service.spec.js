describe('Service: agent group', function () {
  var agentGroup,
    xucAgent,
    preferences,
    agentQueueTotal;

  var agents1 = [
    MockAgentBuilder(1, 'John', 'Snow').build(),
    MockAgentBuilder(2, 'Ann', 'Billy').build(),
    MockAgentBuilder(3, 'Marc', 'Twain').build(),
  ];
  var agents2 = [];
  var totals1 = {
      "sum": {
        "AgentsLoggedOn": 32
      },
      "total": {
        "InbCallTime": 32
      }
    },
    totals2 = {};

  var groupdefs = [{"name": "Cars","queueids": [33, 54]},
                   {"name": "Planes","queueids": [11, 17]}];

  beforeEach(angular.mock.module('panels'));

  beforeEach(angular.mock.inject(function (_AgentGroup_, _XucAgent_, _Preferences_, _AgentQueueTotal_) {
    agentGroup = _AgentGroup_;
    xucAgent = _XucAgent_;
    preferences = _Preferences_;
    agentQueueTotal = _AgentQueueTotal_;
  }));

  it('can instanciate service', function () {
    expect(agentGroup).not.toBeUndefined();
  });

  it('transform queue definition on agent group on get groups', function () {
    spyOn(preferences, 'getQueueGroups').and.returnValue(groupdefs);
    spyOn(xucAgent, 'getAgentsInQueueByIds').and.returnValue(agents1, agents2);
    spyOn(agentQueueTotal, 'calculate').and.returnValue(totals1);

    agentGroup.getGroups();

    expect(preferences.getQueueGroups).toHaveBeenCalledWith('queuegroup');
    expect(xucAgent.getAgentsInQueueByIds).toHaveBeenCalledWith(groupdefs[0].queueids);
    expect(agentQueueTotal.calculate).toHaveBeenCalled();

    expect(agentGroup.getGroups()[0].name).toEqual('Cars');
    expect(agentGroup.getGroups()[0].agents).toEqual(agents1);
    expect(agentGroup.getGroups()[0].totals).toEqual(totals1);

  });

});