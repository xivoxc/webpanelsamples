'use strict';

describe('Service: queuegroup', function () {
  var queueGroup;
  var pref;
  var xucQueue;
  var panelQueueTotal;

  var queues1 = [
    QueueBuilder('blue','blue ocean').build(),
    QueueBuilder('green','green village').build()
  ];
  var groupdefs=[
    {
      "name": "Cars",
      "queueids" : [queues1[0].id,queues1[1].id]
    },
    {
      "name": "Planes",
      "queueids" : [11,17]
    }
  ];

  var queues2 = [];
  var totals1 = {};
  totals1.sum = {};
  totals1.max = {};
  totals1.global = {};

  totals1.sum.TotalNumberCallsEntered = 1;
  totals1.sum.TotalNumberCallsAnswered = 0;
  totals1.sum.TotalNumberCallsAbandonned = 0;
  totals1.sum.TotalNumberCallsClosed = 0;
  totals1.sum.TotalNumberCallsTimeout = 0;
  totals1.sum.WaitingCalls = 0;
  totals1.sum.TotalNumberCallsAnsweredBefore15 = 0;
  totals1.sum.TotalNumberCallsAbandonnedAfter15 = 0;
  totals1.global.PercentageAnsweredBefore15 = 0;
  totals1.global.PercentageAbandonnedAfter15 = 0;
  totals1.max.LongestWaitTime = 0;
  totals1.max.EWT = 0;

  var totals2 = {};

  beforeEach(angular.mock.module('panels'));

  beforeEach(angular.mock.inject(function(_QueueGroup_, _Preferences_, _XucQueue_, _PanelQueueTotal_) {
    pref = _Preferences_;
    xucQueue = _XucQueue_;
    panelQueueTotal = _PanelQueueTotal_;
    spyOn(pref,'getQueueGroups').and.returnValue(groupdefs);
    spyOn(pref,'getQueueGroupsTitle').and.returnValue("Cars");
    queueGroup = _QueueGroup_;
  }));

  it('can instanciate service', function(){
    expect(queueGroup).not.toBeUndefined();
  });

  it('gets groups of queues from preferences on init', function(){
    queueGroup.init();
    expect(pref.getQueueGroups).toHaveBeenCalled();
    expect(pref.getQueueGroupsTitle).toHaveBeenCalled();
  });


  it('transform queue definition on queue group on init', function(){
    spyOn(xucQueue,'getQueueByIds').and.returnValue(queues1, queues2);
    spyOn(panelQueueTotal,'calculate');
    spyOn(panelQueueTotal,'getCalcStats').and.returnValue(totals1, totals2);
    queueGroup.init();
    expect(pref.getQueueGroups).toHaveBeenCalledWith('queuegroup');
    expect(xucQueue.getQueueByIds).toHaveBeenCalledWith(groupdefs[0].queueids);
    expect(panelQueueTotal.calculate).toHaveBeenCalled();
    expect(panelQueueTotal.getCalcStats).toHaveBeenCalled();
    
    expect(queueGroup.getGroups()[0].name).toEqual('Cars');
    expect(queueGroup.getGroups()[0].queues).toEqual(queues1);
    expect(queueGroup.getGroups()[0].totals).toEqual(totals1);

  });

});
