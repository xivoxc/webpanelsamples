'use strict';

describe('Service: preferences', function () {
  var preferences,
    $httpBackend,
    $rootScope;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('panels'));
  
  beforeEach(angular.mock.inject(function (_$rootScope_, _Preferences_, _$httpBackend_) {
    preferences = _Preferences_;
    $httpBackend = _$httpBackend_;
    $rootScope = _$rootScope_;
    TestHelpers.stubPanelsModuleInit($httpBackend);
  }));

  it('can instantiate service', function () {
    expect(preferences).not.toBeUndefined();
  });

  it('retrieve queue group configuration', function () {
    var groupDef = {
      "title": "Transfert",
      "queues": [{
        "name": "Cars",
        "queueids": [1, 7]
      },
      {
        "name": "Planes",
        "queueids": [171, 11]
      }
      ]
    };
    
    
    var jsonAnswer = [{
      "name": "queuegroup",
      "group": groupDef
    }];

    $httpBackend.whenGET('data/queuegroups.json?time='+Date.now()).respond(JSON.stringify(jsonAnswer));
    $httpBackend.expectGET('data/queuegroups.json?time='+Date.now());
    
    preferences.init();
    $httpBackend.flush();
    $rootScope.$apply();
     
    expect(preferences.getGroupDefinitions('queuegroup')).toEqual(groupDef);
    expect(preferences.getQueueGroups('queuegroup')).toEqual(groupDef.queues);
    expect(preferences.getQueueGroupsTitle('queuegroup')).toEqual(groupDef.title);
  });

  it('should return empty queue definitions if group does not exists', function () {
    expect(preferences.getQueueGroups('notexists')).toEqual([]);
  });

  it('should return empty title definitions if group does not exists', function () {
    expect(preferences.getQueueGroupsTitle('notexists')).toEqual([]);
  });

  it('filter only connected agent', function () {
    preferences.setFilterAgentState('connected');
    var testResult = [{
      state: "Pause",
      filtered: true
    },
    {
      state: "AgentReady",
      filtered: true
    },
    {
      state: "AgentLoggedOut",
      filtered: false
    }
    ];

    for (var i = 0; i < testResult.length; i++) {
      var item = testResult[i];
      var agent = MockAgentBuilder(1, 'Marc', 'Twain').onState(item.state).build();
      expect(preferences.agentLoggedOnFilter(agent)).toEqual(item.filtered);
    }

  });

  it('filter all the agents', function () {
    preferences.setFilterAgentState('all');
    var testResult = [{
      state: "Pause",
      filtered: true
    },
    {
      state: "AgentReady",
      filtered: true
    },
    {
      state: "AgentLoggedOut",
      filtered: true
    }
    ];

    for (var i = 0; i < testResult.length; i++) {
      var item = testResult[i];
      var agent = MockAgentBuilder(1, 'Marc', 'Twain').onState(item.state).build();
      expect(preferences.agentLoggedOnFilter(agent)).toEqual(item.filtered);
    }

  });

  it('should stores the selected queue name preference', function () {
    var selectedQueueName = 'th2';
    preferences.setSelectedQueueName(selectedQueueName);
    expect(preferences.getSelectedQueueName()).toEqual(selectedQueueName);
  });
});
