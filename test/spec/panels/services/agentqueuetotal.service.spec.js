'use strict';

describe('Service: AgentQueueTotal', function () {
  var agentQueueTotal;

  beforeEach(angular.mock.module('panels'));

  beforeEach(angular.mock.inject(function(_AgentQueueTotal_) {
    agentQueueTotal = _AgentQueueTotal_;
  }));

  it('can instanciate service', function(){
    expect(agentQueueTotal).not.toBeUndefined();
  });

  it('calculate total of agent in call', function() {
    var agents = [
      MockAgentBuilder(1,'John','Snow').onState('AgentOnCall').build(),
      MockAgentBuilder(2,'Alice','Billy').onState('AgentOnIncomingCall').build(),
      MockAgentBuilder(3,'Betty','Boop').onState('AgentOnOutgoingCall').build(),
      MockAgentBuilder(4,'Kevyn','Array').onState('AgentOnAcdCall').build(),
      MockAgentBuilder(5,'Charlie','Parker').onState('AgentOnPause').build()
    ];

    expect(agentQueueTotal.calculate(agents).sum.AgentsOnCall).toBe(4);

  });

  it('calculate total of agent available', function(){
    var agents = [
      MockAgentBuilder(1,'John','Snow').onState('AgentReady').build(),
      MockAgentBuilder(2,'Alice','Billy').onState('AgentOnIncomingCall').build(),
      MockAgentBuilder(3,'Betty','Boop').onState('AgentOnOutgoingCall').build(),
      MockAgentBuilder(4,'Kevyn','Array').onState('AgentOnWrapup').build(),
      MockAgentBuilder(5,'Charlie','Parker').onState('AgentOnPause').build(),
      MockAgentBuilder(6,'Don','Petrillo').onState('AgentReady').build()
    ];
    expect(agentQueueTotal.calculate(agents).sum.AgentsAvailable).toBe(2);

  });
  it('calculate total of agent available', function(){
    var agents = [
      MockAgentBuilder(1,'John','Snow').onState('AgentLoggedOut').build(),
      MockAgentBuilder(2,'Alice','Billy').onState('AgentOnIncomingCall').build(),
      MockAgentBuilder(3,'Betty','Boop').onState('AgentOnOutgoingCall').build(),
      MockAgentBuilder(4,'Kevyn','Array').onState('AgentOnWrapup').build(),
      MockAgentBuilder(5,'Charlie','Parker').onState('AgentOnPause').build(),
      MockAgentBuilder(6,'Don','Petrillo').onState('AgentReady').build()
    ];
    expect(agentQueueTotal.calculate(agents).sum.AgentsLoggedOn).toBe(5);

  });

  it('calculate total of agent on pause', function(){
    var agents = [
      MockAgentBuilder(1,'John','Snow').onState('AgentLoggedOut').build(),
      MockAgentBuilder(2,'Alice','Billy').onState('AgentOnIncomingCall').build(),
      MockAgentBuilder(3,'Betty','Boop').onState('AgentOnPause').build(),
      MockAgentBuilder(4,'Kevyn','Array').onState('AgentOnWrapup').build(),
      MockAgentBuilder(5,'Charlie','Parker').onState('AgentOnPause').build(),
      MockAgentBuilder(6,'Don','Petrillo').onState('AgentReady').build()
    ];
    expect(agentQueueTotal.calculate(agents).sum.AgentsOnPause).toBe(2);

  });

  it('calculate number of outgoing calls', function(){
    var agents = [
      MockAgentBuilder(1,'John','Snow').onState('AgentLoggedOut').withStat('OutCalls', 2).build(),
      MockAgentBuilder(2,'Alice','Billy').onState('AgentOnIncomingCall').withStat('OutCalls', 1).build(),
      MockAgentBuilder(3,'Betty','Boop').onState('AgentOnPause').build(),
      MockAgentBuilder(4,'Kevyn','Array').onState('AgentOnWrapup').build(),
      MockAgentBuilder(5,'Charlie','Parker').onState('AgentOnPause').build(),
      MockAgentBuilder(6,'Don','Petrillo').onState('AgentReady').build()
    ];
    expect(agentQueueTotal.calculate(agents).sum.OutgoingCalls).toBe(3);

  });

});
