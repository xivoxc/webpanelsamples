'use strict';

describe('Controller: queue', function () {

  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('panels'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(angular.mock.module('Th2'));
  
  var ctrl,
    scope,
    queueSelector,
    $q;

  beforeEach(angular.mock.inject(function ($controller, $rootScope, _QueueSelector_, _$q_) {
    
    scope = $rootScope.$new();
    queueSelector = _QueueSelector_;
    $q = _$q_;

    
    ctrl = $controller('QueueController', {
      $scope: scope,
      QueueSelector: queueSelector
    });
  }));

  it('can instanciate controller', function () {
    expect(ctrl).not.toBeUndefined();
  });
  
  it('can load queues', function () {
    var queues = [QueueBuilder('commercial', 'commercial accounts').build()];

    var mockGetQueues = $q.defer();
    mockGetQueues.resolve(queues);


    spyOn(queueSelector, "getQueues").and.returnValue(mockGetQueues.promise);

    ctrl.getQueues();

    scope.$digest();
    expect(ctrl.queues).toEqual(queues);
  });
});