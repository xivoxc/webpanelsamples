'use strict';

describe('Service: dataFileLoader', function () {
  var dataFileLoader;
  var $httpBackend;
  var $rootScope;

  beforeEach(angular.mock.module('Th2'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(angular.mock.inject(function (_DataFileLoader_, _$rootScope_, _$httpBackend_) {
    dataFileLoader = _DataFileLoader_;
    $httpBackend = _$httpBackend_;
    $rootScope = _$rootScope_;
    TestHelpers.stubPanelsModuleInit($httpBackend);
  }));

  it('can instantiate service', function () {
    expect(dataFileLoader).not.toBeUndefined();
  });

  it("should return queues list from selectQueues.json", function () {
    var th2Names = ['commercial', 'sell'];
    var jsonAnswer = [{
      "name": "Th1",
      "queues": [
        "sell",
        "premium"
      ]
    },
    {
      "name": "Th2",
      "queues": th2Names
    },
    ];

    $httpBackend.whenGET('data/selectQueues.json').respond(JSON.stringify(jsonAnswer));
    $httpBackend.expectGET('data/selectQueues.json');

    dataFileLoader.loadQueuesList('Th2').then(function (resultNames) {
      expect(resultNames).toEqual(th2Names);
    });
    $httpBackend.flush();
    $rootScope.$apply();
  });
});
