'use strict';

describe('Service: queueSelector', function () {
  var queueSelector;
  var preferences;
  var dataFileLoader;
  var xucQueue;
  var objectSorter;
  var $q;
  var $rootScope;

  beforeEach(angular.mock.module('Th2'));
  beforeEach(angular.mock.module('panels'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('karma-backend'));
  
  beforeEach(angular.mock.inject(function (_QueueSelector_, _Preferences_, _DataFileLoader_, _XucQueue_, _ObjectSorter_, _$q_, _$rootScope_) {
    queueSelector = _QueueSelector_;
    preferences = _Preferences_;
    dataFileLoader = _DataFileLoader_;
    xucQueue = _XucQueue_;
    objectSorter = _ObjectSorter_;
    $q = _$q_;
    $rootScope = _$rootScope_;
  }));

  it('can instanciate service', function () {
    expect(queueSelector).not.toBeUndefined();
  });

  it('return an corretly ordre list of queues', function () {

    var name = 'Th2';
    var queueNames = ["commercial", "sell", "premium"];

    var commercial = QueueBuilder('commercial', 'commercial accounts').build();
    var premium = QueueBuilder('premium', 'premium customers').build();
    var sell = QueueBuilder('sell', 'sell demer').build();

    var availableQueues = [
      sell,
      QueueBuilder('blue', 'blue ocean').build(),
      commercial,
      QueueBuilder('green', 'green village').build(),
      premium
    ];

    var queueResult = [commercial, sell, premium];

    var pLoadQueuesList = $q.defer();
    pLoadQueuesList.resolve(queueNames);

    var pGetQueues = $q.defer();
    pGetQueues.resolve(availableQueues);

    spyOn(preferences, 'getSelectedQueueName').and.returnValue(name);
    spyOn(dataFileLoader, 'loadQueuesList').and.returnValues(pLoadQueuesList.promise);
    spyOn(xucQueue, 'getQueuesAsync').and.returnValues(pGetQueues.promise);
    spyOn(objectSorter, 'sort').and.returnValues(queueResult);

    var loadedQueues;


    queueSelector.getQueues().then(function (q) {
      loadedQueues = q;
    });

    $rootScope.$digest();

    expect(objectSorter.sort).toHaveBeenCalledWith(queueNames, availableQueues);
    expect(preferences.getSelectedQueueName).toHaveBeenCalled();
    expect(dataFileLoader.loadQueuesList).toHaveBeenCalledWith(name);


    expect(loadedQueues).toEqual(queueResult);
  });

  it('should return all the xuc queue if no preference queue filter is init', function () {
    var availableQueues = [
      QueueBuilder('sell', 'sell demer').build(),
      QueueBuilder('blue', 'blue ocean').build(),
      QueueBuilder('commercial', 'commercial accounts').build(),
      QueueBuilder('green', 'green village').build(),
      QueueBuilder('premium', 'premium customers').build()
    ];
    spyOn(preferences, 'getSelectedQueueName').and.returnValue('');

    var pGetQueues = $q.defer();
    pGetQueues.resolve(availableQueues);
    spyOn(xucQueue, 'getQueuesAsync').and.returnValues(pGetQueues.promise);

    var loadedQueues;
    queueSelector.getQueues().then(function (q) {
      loadedQueues = q;
    });

    $rootScope.$digest();

    expect(loadedQueues).toEqual(availableQueues);
  });
});