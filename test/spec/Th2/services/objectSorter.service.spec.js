'use strict';

describe('Service: objectSorter', function () {
  var objectSorter;


  beforeEach(angular.mock.module('Th2'));

  beforeEach(angular.mock.inject(function (_ObjectSorter_) {
    objectSorter = _ObjectSorter_;
  }));

  it('can instanciate service', function () {
    expect(objectSorter).not.toBeUndefined();
  });

  var filter = ["technique", "sell", "premium"];
  var list = [{
    "id": 5,
    "name": "sell",
    "displayName": "Sell",
    "number": "3002"
  },
  {
    "id": 4,
    "name": "support",
    "displayName": "Support",
    "number": "3001"
  },
  {
    "id": 8,
    "name": "technique",
    "displayName": "Technique",
    "number": "3005"
  },
  {
    "id": 11,
    "name": "premium",
    "displayName": "Premium",
    "number": "3008"
  }
  ];
  var result = [{
    "id": 8,
    "name": "technique",
    "displayName": "Technique",
    "number": "3005"
  },
  {
    "id": 5,
    "name": "sell",
    "displayName": "Sell",
    "number": "3002"
  },
  {
    "id": 11,
    "name": "premium",
    "displayName": "Premium",
    "number": "3008"
  }
  ];


  it('should return a list filter by filter and sort by filter order', function () {
    expect(objectSorter.sort(filter, list)).toEqual(result);
  });

  it('should return the list if filter is null', function () {
    expect(objectSorter.sort(null, list)).toEqual(list);
  });


});