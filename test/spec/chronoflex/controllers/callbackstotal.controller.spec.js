'use strict';

describe('Controller: CallbacksTotal', function () {

  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('panels'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(angular.mock.module('chronoflex'));
  
  var ctrl, scope, rootScope;
  var XucCallback = jasmine.createSpyObj('XucCallback', ['refreshCallbacks']);
  var callbackList = [{queueId:1, callbacks:[{id: 1}, {id:2}]}, {queueId:2, callbacks: [{id:11}]}];
  beforeEach(angular.mock.inject(function ($controller, $rootScope) {
    
    scope = $rootScope.$new();
    rootScope = $rootScope;
    XucCallback.getCallbackLists = function() {return callbackList; };
    XucCallback.getCallbackListsAsync = function() {};

    ctrl = $controller('CallbacksTotalController', {
      XucCallback: XucCallback,
      $scope: scope,
      $rootScope: rootScope
    });
  }));

  it('can instantiate controller', function () {
    expect(ctrl).not.toBeUndefined();
  });

  it('refreshes callbacks on CallbacksLoaded broadcast', function() {
    spyOn(XucCallback, 'getCallbackListsAsync').and.callThrough();

    scope.queues = [{id: 1}];
    scope.$apply();
    expect(XucCallback.getCallbackListsAsync).toHaveBeenCalled();
  });

  it("loads correctly callbacks count when there's no queue filter", function() {
    rootScope.queues=[{id:1}, {id:2}];
    ctrl.refreshCallbacks();
    expect(scope.callbacksCount).toBe(3);
  });
  
  it("loads correctly callbacks count when one queue is selected", function() {
    rootScope.queues=[{id:1}];
    ctrl.refreshCallbacks();
    expect(scope.callbacksCount).toBe(2);
  });

});