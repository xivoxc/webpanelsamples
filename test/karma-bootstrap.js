'use strict';

Cti.init  = function() {
};

var karmaBackend = angular.module('karma-backend', []);

karmaBackend.run(function($httpBackend) {
  var config = {
    "host" : "localhost",
    "port" : "8090",
    "username" : "bwillis",
    "password" : "1234",
    "description" : "do not remove"
  };

  $httpBackend.whenGET('./config/config.json').respond(JSON.stringify(config));
});