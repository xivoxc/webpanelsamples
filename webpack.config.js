const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = {
  entry: {
    panels: ['./app/scripts/panels/panels-webpack.js'],
    xccti: ['./app/scripts/xccti/cti-webpack.js'],
    globaloptpanel: ['./app/scripts/globaloptpanel/app.js'],
    lfm: ['./app/scripts/lfm/app.js'],
    blackboard: ['./app/scripts/blackboard/app.js'],
    simple: ['./app/scripts/simple/app.js'],
    summarylogo: ['./app/scripts/summarylogo/app.js'],
    perqueuelogo: ['./app/scripts/perqueuelogo/app.js'],
    th1: ['./app/scripts/Th1/app.js'],
    th2: ['./app/scripts/Th2/app.js'],
    ugps: ['./app/scripts/ugps/app.js'],
    qma: ['./app/scripts/qma/app.js'],
    ugps_2: ['./app/scripts/ugps_2/app.js'],
    ugps_3: ['./app/scripts/ugps_3/app.js'],
    listqueue: ['./app/scripts/listqueue/app.js'],
    hbt76: ['./app/scripts/hbt76/app.js'],
    edv: ['./app/scripts/edv/app.js'],
    chronoflex: ['./app/scripts/chronoflex/app.js'],
    itelis: ['./app/scripts/itelis/app.js'],
    sogetrel: ['./app/scripts/sogetrel/app.js'],
    ccsupervision: ['./app/scripts/ccsupervision/app.js'],
    index: ['./app/scripts/index.js'],
    vendor: ['bootstrap', 'bootstrap-select']
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'webdist')
  },
  resolve: {
    alias: {
      "panels": path.resolve(__dirname, "app/scripts/panels/"),
      "xccti": path.resolve(__dirname, "app/scripts/xccti/"),
      "globaloptpanel": path.resolve(__dirname, "app/scripts/globaloptpanel/"),
      "lfm": path.resolve(__dirname, "app/scripts/lfm/"),
      "blackboard": path.resolve(__dirname, "app/scripts/blackboard/"),
      "simple": path.resolve(__dirname, "app/scripts/simple/"),
      "summarylogo": path.resolve(__dirname, "app/scripts/summarylogo/"),
      "perqueuelogo": path.resolve(__dirname, "app/scripts/perqueuelogo/"),
      "hbt76": path.resolve(__dirname, "app/scripts/hbt76/"),
      "th1": path.resolve(__dirname, "app/scripts/Th1/"),
      "th2": path.resolve(__dirname, "app/scripts/Th2/"),
      "ugps": path.resolve(__dirname, "app/scripts/ugps/"),
      "qma": path.resolve(__dirname, "app/scripts/qma/"),
      "ugps_2": path.resolve(__dirname, "app/scripts/ugps_2/"),
      "ugps_3": path.resolve(__dirname, "app/scripts/ugps_3/"),
      "edv": path.resolve(__dirname, "app/scripts/edv/"),
      "chronoflex": path.resolve(__dirname, "app/scripts/chronoflex/"),
      "itelis": path.resolve(__dirname, "app/scripts/itelis/"),
      "sogetrel": path.resolve(__dirname, "app/scripts/sogetrel/"),
      "ccsupervision": path.resolve(__dirname, "app/scripts/ccsupervision/"),
      "listqueue": path.resolve(__dirname, "app/scripts/listqueue/")
    }
  },
  devServer: {
    contentBase: path.join(__dirname, "webdist"),
  },
  devtool: 'inline-source-map',
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      moment: 'moment',
      _: 'lodash',
      "window.jQuery": "jquery",
      "window._": "lodash"
    }),
    new ExtractTextPlugin({filename: '[name].styles.css', allChunks: true}),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: function (module) {
        return module.context && module.context.indexOf('node_modules') !== -1;
      }
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'manifest'
    }),
    new CopyWebpackPlugin([
      {from: 'app/data', to: 'data' },
      {from: 'app/images', to: 'images' },
      {from: './app/scripts/xccti/js/cti.js', to: 'cti.js'},
      {from: './app/scripts/xccti/js/callback.js', to: 'callback.js'},
      {from: './app/index.html', to: 'index.html'},
      {from: './app/globaloptpanel.html', to: 'globaloptpanel.html'},
      {from: './app/optpanel.html', to: 'optpanel.html'},
      {from: './app/lfm.html', to: 'lfm.html'},
      {from: './app/blackboard.html', to: 'blackboard.html'},
      {from: './app/simple.html', to: 'simple.html'},
      {from: './app/summarylogo.html', to: 'summarylogo.html'},
      {from: './app/perqueuelogo.html', to: 'perqueuelogo.html'},
      {from: './app/Th1.html', to: 'Th1.html'},
      {from: './app/Th2.html', to: 'Th2.html'},
      {from: './app/ugps.html', to: 'ugps.html'},
      {from: './app/qma.html', to: 'qma.html'},
      {from: './app/ugps_2.html', to: 'ugps_2.html'},
      {from: './app/ugps_3.html', to: 'ugps_3.html'},
      {from: './app/listqueue.html', to: 'listqueue.html'},
      {from: './app/hbt76.html', to: 'hbt76.html'},
      {from: './app/edv.html', to: 'edv.html'},
      {from: './app/chronoflex.html', to: 'chronoflex.html'},
      {from: './app/itelis.html', to: 'itelis.html'},
      {from: './app/sogetrel.html', to: 'sogetrel.html'},
      {from: './app/ccsupervision.html', to: 'ccsupervision.html'},
      {from: './app/config', to: 'config'}
    ]),
    new CleanWebpackPlugin(['webdist'])
  ],
  module: {
    rules: [{
      enforce: "pre",
      test: /\.js$/,
      include: [/th2/, /th1/, /panels/, /index/],
      exclude: [/node_modules/, /cti.js/, /xuc_services.js/],
      loader: "eslint-loader"
    },
    {
      test: require.resolve('jquery'),
      loader: 'expose-loader?jQuery!expose-loader?$'
    },
    {
      test: require.resolve('angular'),
      loader: 'expose-loader?angular!expose-loader?angular'
    },
    {
      test: /\.js$/,
      include: [/th2/, /th1/, /panels/, /xccti/, /index/],
      exclude: [/node_modules/],
      use: {
        loader: 'eslint-loader',
        options: {
          presets: ['es2015'],
          cacheDirectory: true
        }
      }
    },
    {
      test: /\.(png|jpg|gif)$/,
      use: [
        {
          loader: 'file-loader',
          options: {}
        }
      ]
    },
    {
      test: /\.woff2?$|\.ttf$|\.eot$|\.svg$/,
      use: [{
        loader: "file-loader"
      }]
    },
    {
      test: /\.css$/,
      use: ExtractTextPlugin.extract({
        fallback: "style-loader",
        use: "css-loader"
      })
    }
    ]
  }
};