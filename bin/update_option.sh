#!/bin/bash

optfile=/usr/share/webpanels/config/config.json


function replace() {
  if [[ "$1" != "" ]]; then
      echo "$2 is $1"
      sed -i "s/\"$2\".*,/\"$2\" : \""$1"\",/" $optfile
  fi
}

replace "$XUC_HOST" "host"
replace "$XUC_PORT" "port"
replace "$XUC_USERNAME" "username"
replace "$XUC_PASSWORD" "password"